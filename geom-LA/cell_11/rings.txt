# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 11
# --------------------------------------------------------------------------------------------------------------------

############ Rings surface definition ############

# All units in microns and seconds

panel disk -0.15 0 0 0.7 -1 0 0 10
panel disk 0.15 0 0 0.7 1 0 0 10
panel disk -0.95 0 0 0.7 -1 0 0 10
panel disk -0.65 0 0 0.7 1 0 0 10
panel disk 0.95 0 0 0.7 1 0 0 10
panel disk 0.65 0 0 0.7 -1 0 0 10

end_file
