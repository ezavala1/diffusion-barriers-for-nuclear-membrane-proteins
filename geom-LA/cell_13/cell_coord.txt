# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 13
# --------------------------------------------------------------------------------------------------------------------

############ Compartment-defining coordinates ############

# All units in microns and seconds

define MCP -2.5
define DCP 2.5
define BCPx -3.01
define BCPy -0.74

compartment motherOut point MCP 0 0
compartment daughterOut point DCP 0 0
compartment motherIn point MCP 0 0
compartment daughterIn point DCP 0 0
compartment bleachspot point BCPx BCPy 0

end_file
