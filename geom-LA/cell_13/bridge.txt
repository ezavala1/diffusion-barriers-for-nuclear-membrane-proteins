# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 13
# --------------------------------------------------------------------------------------------------------------------

############ bridge surface definition ############

# All units in microns and seconds

panel disk -1.85 0 0 0.7 -1 0 0 10
panel disk 1.85 0 0 0.7 1 0 0 10

end_file
