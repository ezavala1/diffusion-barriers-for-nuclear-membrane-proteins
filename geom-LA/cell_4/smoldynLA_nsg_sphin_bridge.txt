# SMOLDYN 2.28 file
# Simple bouncing molecules with surfaces for testing yeast dumbbell nucleus in LATE anaphase
# This file requires surface-defining files LABleach.txt, LADumbbellIn.txt and LADumbbellOut.txt in same folder
# (see Mathematica file yeastgeomLA.nb for source wrl files / see Python script wrltotxtLA.py for generating surfaces)
# --------------------------------------------------------------------------------------------------------------------

# All units in microns and seconds

############ Global definitions ############

define TSTEP 0.00004   #Spatial-resolution = 7 nm for Diffusion rates up to 0.3 u^2/s (p17 Smoldyn manual)

define TSTART 0
define TSTOP 200      #Equals time-span of FLIP experiments + T_THERM
define TSAMPLING 2

define T_THERM 0.001       #Thermalization time needed for the molecules to 'settle'

define RADII 0.008

define DIFCOEF 0.3
define DIFCOEF_BARR 0.1

define PERM_IN 0.15
define PERM_OUT 1

############ Initial settings ############

graphics opengl_good
graphic_iter 1
cmd b pause           #comment this line if using cluster

############ System settings (space and time) ############

dim 3

# Frame must be big enough to include both lobes, if it doesn't, increase boundaries
boundaries 0 -5 5 
boundaries 1 -2 2
boundaries 2 -2 2
frame_thickness 0
grid_thickness 0
background_color black

time_start TSTART
time_stop TSTOP      #Equals time-span of FLIP experiments + T_THERM
time_step TSTEP

############ Molecules ############

species nsg temp_bleaching bleached barr_nsg barr_bleached

difc nsg(all) DIFCOEF
difc temp_bleaching(all) DIFCOEF
difc bleached(all) DIFCOEF

difc barr_nsg(all) DIFCOEF_BARR
difc barr_bleached(all) DIFCOEF_BARR

color nsg(all) lightgreen
color bleached(all) darkgreen
color barr_nsg(all) orange
color barr_bleached(all) orange

display_size all(front) RADII

text_display time nsg(front) bleached(front) barr_nsg(front) barr_bleached(front)

############ Surfaces ############

start_surface LADumbbellOut
color both 0.2 0.2 0.2
polygon both edge
read_file LADumbbellOut.txt
end_surface

start_surface LABleach
rate_internal nsg(front) bsoln fsoln 1 temp_bleaching
rate_internal temp_bleaching(front) fsoln bsoln 1 nsg
color both 0.2 0.5 0.5
polygon both edge
read_file LABleach.txt
end_surface

start_surface sphin_barrier
rate_internal nsg(front) fsoln bsoln PERM_IN barr_nsg
rate_internal barr_nsg(front) bsoln fsoln PERM_OUT nsg
rate_internal bleached(front) fsoln bsoln PERM_IN barr_bleached
rate_internal barr_bleached(front) bsoln fsoln PERM_OUT bleached
color both 0 0 0
polygon both edge
read_file bridge.txt
end_surface

############ Initial conditions ############

surface_mol 1900 nsg(front) LADumbbellOut all all

############ Reactions ############

# Bleaching:
cmd @ T_THERM set reaction_surface LADumbbellOut bleaching temp_bleaching(front) -> bleached(front) 150

############ Movie ############

#tiff_iter 100000    # Choose 1e5 for 1 tiff frame per second in a 200 seconds simulation (aprox 5 MB per tiff file)

############ Output ############

output_files outputLA_M.txt outputLA_D.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox -5 0 -2 2 -2 2 outputLA_M.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox 0 5 -2 2 -2 2 outputLA_D.txt

end_file
