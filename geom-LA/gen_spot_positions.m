%% Script for generating surface-defining files that locate the bleaching spot at different positions along the bridge in LA

clear

% Load the .txt file that lists the center positions of the spot for each cell
load locations.txt

% Create 11 position files for each cell
for cell = 1:34
  
  for i = 1:11
  
    filename = strcat('cell_',num2str(cell),'/spot_',num2str(i),'.txt');
  
    fid = fopen(filename,'wt');
      fprintf(fid,'panel sph %f 0 0 -0.2 20 20\n',locations(cell,i));
      fprintf(fid,'end_file\n');
    fclose(fid);
  
  end
  
end
