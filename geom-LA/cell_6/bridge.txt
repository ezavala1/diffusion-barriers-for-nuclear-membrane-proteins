# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 6
# --------------------------------------------------------------------------------------------------------------------

############ bridge surface definition ############

# All units in microns and seconds

panel disk -2.25 0 0 0.7 -1 0 0 10
panel disk 2.25 0 0 0.7 1 0 0 10

end_file
