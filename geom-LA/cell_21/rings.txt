# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 21
# --------------------------------------------------------------------------------------------------------------------

############ Rings surface definition ############

# All units in microns and seconds

panel disk -0.15 0 0 0.7 -1 0 0 10
panel disk 0.15 0 0 0.7 1 0 0 10
panel disk -0.90 0 0 0.7 -1 0 0 10
panel disk -0.60 0 0 0.7 1 0 0 10
panel disk -1.65 0 0 0.7 -1 0 0 10
panel disk -1.35 0 0 0.7 1 0 0 10
panel disk 0.90 0 0 0.7 1 0 0 10
panel disk 0.60 0 0 0.7 -1 0 0 10
panel disk 1.65 0 0 0.7 1 0 0 10
panel disk 1.35 0 0 0.7 -1 0 0 10

end_file
