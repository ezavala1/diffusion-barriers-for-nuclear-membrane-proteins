# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 3
# --------------------------------------------------------------------------------------------------------------------

############ bridge surface definition ############

# All units in microns and seconds

panel disk -1.15 0 0 0.7 -1 0 0 10
panel disk 1.15 0 0 0.7 1 0 0 10

end_file
