# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 17
# --------------------------------------------------------------------------------------------------------------------

############ Rings surface definition ############

# All units in microns and seconds

panel disk -0.15 0 0 0.7 -1 0 0 10
panel disk 0.15 0 0 0.7 1 0 0 10
panel disk -0.80 0 0 0.7 -1 0 0 10
panel disk -0.50 0 0 0.7 1 0 0 10
panel disk -1.45 0 0 0.7 -1 0 0 10
panel disk -1.15 0 0 0.7 1 0 0 10
panel disk 0.80 0 0 0.7 1 0 0 10
panel disk 0.50 0 0 0.7 -1 0 0 10
panel disk 1.45 0 0 0.7 1 0 0 10
panel disk 1.15 0 0 0.7 -1 0 0 10

end_file
