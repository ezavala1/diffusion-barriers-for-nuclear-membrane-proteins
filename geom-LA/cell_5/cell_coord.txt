# SMOLDYN 2.26 definition file

# LATE anaphase

# Cell 5
# --------------------------------------------------------------------------------------------------------------------

############ Compartment-defining coordinates ############

# All units in microns and seconds

define MCP -2.5
define DCP 2.5
define BCPx -2.28
define BCPy -0.73

compartment motherOut point MCP 0 0
compartment daughterOut point DCP 0 0
compartment motherIn point MCP 0 0
compartment daughterIn point DCP 0 0
compartment bleachspot point BCPx BCPy 0

end_file
