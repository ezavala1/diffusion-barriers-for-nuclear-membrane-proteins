%% SCRIPT FOR EDITING DELAUNAY HULLS AND CORRECT UNDESIRED TRIANGLES

clear
addpath(['/Users/zavalaeder/Dropbox/Yeast/'])    % Set path to original data file

%% Read data of triangular panels as defined in original file

source = fopen('EADumbbellOut.txt','r');    % Open original data file and set permissions (e.g. read, write, etc.)
panels = textscan(source,'%*s %*s %f %f %f %f %f %f %f %f %f tri%f','HeaderLines',6,'CommentStyle',{'neighbors','end_file'});

tot_triangles = size(panels{10},1);  % initial total number of triangles

tri_arr = [];    % store triangle vertices and index
for i = 1:10
  tri_arr = cat(2,tri_arr,panels{i}(1:end));
end

%% Read data of neighboring panels as defined in original file

source = fopen('EADumbbellOut.txt','r');    % Open original data file and set permissions (e.g. read, write, etc.)
neighbors = textscan(source,'%*s %s %s %s %s %s %s %s','HeaderLines',tot_triangles + 6,'CommentStyle','end_file');

tot_neighbors = size(neighbors{1},1);  % initial total number of neighbors

if tot_triangles ~= tot_neighbors   % validation of # of triangles must equal # of neighbor definitions
  disp('...error reading neighbors');
end

nei_str = [];    % store neighboring string data
for i = 1:7
  nei_str = cat(2,nei_str,neighbors{i}(1:end));
end

nei_arr = zeros(tot_neighbors,7);    % store neighboring numerical index data
for i = 1:tot_neighbors
  for j = 1:7
    if ~isempty(sscanf(nei_str{i,j},'tri%u'))
      nei_arr(i,j) = sscanf(nei_str{i,j},'tri%u');
    end
  end
end

%% Create destination file and write corrected data in it

destiny = fopen('EADumbbellOut.txt','w');
fprintf(destiny,'%s \n\n','# Smoldyn EDITED surface data file. Corrupt triangles/neighbors are corrected.');
fprintf(destiny,'%s %s %u \n\n','max_panels','tri',tot_triangles);

wrong_triangles = [];    % list of triangles that must be wiped out from original data

for i = 1:tot_triangles
  if tri_arr(i,3)==0 && tri_arr(i,6)==0 && tri_arr(i,9)==0    % spurious triangles
    wrong_triangles = cat(1,wrong_triangles,tri_arr(i,10));
    fprintf(destiny,'%s %s %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %s%u \n','#panel','tri',tri_arr(i,1:9),'tri',tri_arr(i,10));
  else
    fprintf(destiny,'%s %s %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %s%u \n','panel','tri',tri_arr(i,1:9),'tri',tri_arr(i,10));
  end
end

for i = 1:tot_neighbors
  if any(nei_arr(i,1) == wrong_triangles)    % search if the triangle for which its neighbors are defined is a spurious triangle
    fprintf(destiny,'%s %s %s %s %s %s %s %s \n','#neighbors',nei_str{i,:});
  else
    for j = 2:7    % search for if spurious triangles are defined as neighbors in the same row and erase them accordingly
      if any(nei_arr(i,j) == wrong_triangles)
        nei_str{i,j} = '';
      end
    end
    fprintf(destiny,'%s %s %s %s %s %s %s %s \n','neighbors',nei_str{i,:});    % print non-conflictive output only
  end
end

fprintf(destiny,'\n%s\r','end_file');    % \r is important to avoid error: 'line exceeds maximum allowable length of 256 characters'

%% Close files

closure = fclose('all');
if closure == 0, disp('...all files closed correctly'), else disp('...error closing files!!!'), end

%% Converting from Macintosh to Linux format

% When running in Smoldyn, the new edited file will display an error message stating that it's in Macintosh format
% and needs to be changed to Unix. This arises from the use of different line termination characters.
% Execute the following line at a terminal command prompt to convert the file from Macintosh to Linux format:

% perl -pi -e 's/\r/\n/g' filename    % where 'filename' must be substituted by the file name of the edited file

% The following line converts a bunch of files from Macintosh to Linux termination characters:

% for filename in $(find yourdirectory/* -name *.txt); do perl -pi -e 's/\r/\n/g' $filename; done    % this time 'filename' must be written as is
