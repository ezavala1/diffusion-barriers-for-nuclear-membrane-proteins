#!usr/bin/python

##This is a python script that invokes Steve Andrews's wrl2smol, which converts
##several wrl files into a single which are readable by Smoldyn. Use this script 
##if you have a lot of wrl files to convert, and you don't want to convert them
##all manually.

##To use this script, you need to have installed wrl2smol (which is installed as
##a default when installing Smoldyn). You also need Python (available freely:
##http://www.python.org). You also need the Pexpect module for Python (available
##from http://www.noah.org/wiki/pexpect). It suffices to keep the file pexpect

##First import the pexpect module, which is a tool for running programs then
##interacting with them automatedly
import pexpect

##Then import the glob module, which will find wrl files in the directory that
##this script is in
import glob

import sys

###############Script Input Parameters###############

##List the .wrl files you want added to the list below. If you want all .wrl
##files in the directory of the script, leave the list empty, i.e. an open and
##closed set of square brackets, with no space inside.
wrl1=['LABleachBottom.wrl', 'LABleachTop.wrl']
wrl2=['LADumbbellTopIn.wrl', 'LADumbbellBottomIn.wrl']
wrl3=['LADumbbellTopOut.wrl', 'LADumbbellBottomOut.wrl']

##if you want to manually enter the file names, do so as follows:
##wrl=['filename1.wrl', 'filename2.wrl', 'filename3.wrl', ...]
##Please note the space after each comma! 

##Join adjacent but currently disjoint triangles, 'y' or 'n'?
join='y'

#Set the translation vector---don't forget the spaces!
translation='0 0 0'

#Set the scaling vector---don't forget the spaces!
scaling='1 1 1'

#Align orientations of neighbouring triangles, 'y' or 'n'?
align = 'y'

#Flip triangles, 'y' or 'n'?
flip = 'n'

##Provide the output name of the .txt file
outputname1='LABleach.txt'
outputname2='LADumbbellIn.txt'
outputname3='LADumbbellOut.txt'

##Enter name and starting number of triangle
startname='tri 1'

##Print neighbours? Choose 1 for no neighbours, 2 for edge neighbourswrl, 3 for all
neighbours='2'

#####################################################




###############Script Execution###############

#### BLEACH SPOT ####

##We check if wrl is empty. If it is we print an error message
if not wrl1:
				print 'ERROR: user has specified no .wrl files'

##Open wrl2smol with pexpect and interact with it accordingly
child=pexpect.spawn('wrl2smol')
for l in wrl1:
				child.expect('.*: ')	
				child.sendline(l)

child.expect('.*: ')
child.sendline('done')
child.expect('.*: ')
child.sendline(join)
child.expect('.*: ')
child.sendline(translation)
child.expect('.*: ')
child.sendline(scaling)
child.expect('.*: ')
child.sendline(align)
i=1
if len(wrl1)>1:
		while i<len(wrl1):
						child.expect('.*: ')
						child.sendline(flip)
						i=i+1

child.expect('.*: ')
child.sendline(outputname1)
child.expect('.*: ')
child.sendline(startname)
child.expect('.*: ')
child.sendline(neighbours)

#### DUMBBELL IN ####

##We check if wrl is empty. If it is we print an error message
if not wrl2:
				print 'ERROR: user has specified no .wrl files'

##Open wrl2smol with pexpect and interact with it accordingly
child=pexpect.spawn('wrl2smol')
for l in wrl2:
				child.expect('.*: ')	
				child.sendline(l)

child.expect('.*: ')
child.sendline('done')
child.expect('.*: ')
child.sendline(join)
child.expect('.*: ')
child.sendline(translation)
child.expect('.*: ')
child.sendline(scaling)
child.expect('.*: ')
child.sendline(align)
i=1
if len(wrl2)>1:
		while i<len(wrl2):
						child.expect('.*: ')
						child.sendline(flip)
						i=i+1

child.expect('.*: ')
child.sendline(outputname2)
child.expect('.*: ')
child.sendline(startname)
child.expect('.*: ')
child.sendline(neighbours)

#### DUMBBELL OUT ####

##We check if wrl is empty. If it is we print an error message
if not wrl3:
				print 'ERROR: user has specified no .wrl files'

##Open wrl2smol with pexpect and interact with it accordingly
child=pexpect.spawn('wrl2smol')
for l in wrl3:
				child.expect('.*: ')	
				child.sendline(l)

child.expect('.*: ')
child.sendline('done')
child.expect('.*: ')
child.sendline(join)
child.expect('.*: ')
child.sendline(translation)
child.expect('.*: ')
child.sendline(scaling)
child.expect('.*: ')
child.sendline(align)
i=1
if len(wrl3)>1:
		while i<len(wrl3):
						child.expect('.*: ')
						child.sendline(flip)
						i=i+1

child.expect('.*: ')
child.sendline(outputname3)
child.expect('.*: ')
child.sendline(startname)
child.expect('.*: ')
child.sendline(neighbours)



##############################################
