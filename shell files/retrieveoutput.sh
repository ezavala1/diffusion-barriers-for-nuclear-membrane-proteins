## This is a simple Bourne shell script for
## retrieving a series of identical Smoldyn output files
## Note: comment lines start with '#' with some exceptions; 
## e.g. '#!' is NOT a comment. I use '##' to avoid any confusions. 

## Request Bourne shell
#!/bin/sh

## Go to directory where output is stored in subdirectories(one for each job)
cd /work/MarquezU/eder

## As all output files have the same name, retrieve them out of subdirectories
## while renaming them by their job ID
for file in */*.txt; do cp "$file" "${file//\//-}"; done

## Then simply logout from Tombo and do a server-copy of files from Tombo
## to your working directory, for instance:
##scp eder-zavala@tombo.oist.jp:/work/MarquezU/eder/*.txt /Users/zavalaeder/Dropbox/Yeast/geosmol-wtEA/cell_01/output
