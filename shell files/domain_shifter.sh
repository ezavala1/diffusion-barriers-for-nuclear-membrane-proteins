
## This is a simple Bourne shell script for
## submitting a series of jobs to tombo 
## Note: comment lines start with '#' with some exceptions; 
## e.g. '#!' is NOT a comment. I use '##' to avoid any confusions. 

## Request Bourne shell
#!/bin/sh

## loop from i=1 to N
for i in {1..34}
do
 ## go to where i-th slave shell file is contained
 cd cell_$i
 ## backup old .sh file
 cp slave_cell_$i.sh bak_slave_cell_$i.sh
 rm slave_cell_$i.sh
 ## use sed command to replace domain definition filename (new filename must be different)
 ## sed 's/old-word/new-word/g' old-filename.sh > new-filename.sh
 sed 's/rings/bridge/g' bak_slave_cell_$i.sh > slave_cell_$i.sh
 ## go to parent directory
 cd ..
done