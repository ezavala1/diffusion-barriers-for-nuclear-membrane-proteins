#!/bin/sh
##A shell script for repeating instances of a Smoldyn simulation described in a file called BimolecularTest.txt by editing the Smoldyn configuration file, running it, then editing, then running, etc.

##Define the number of repeats
repeats="1000"

##Dummy variable to denote the number of the simulation run. This is also used to label files
run="1"

##While the run number is less than the number of repeats
while
	(( $run <= $repeats ))
do
##Substitute everything on line 30 with output_files bimolresulsRUN#.txt to change the configuration file so that the output file is labelled according to the simulation run.
sed -i '' "30s/.*/output_files bimolresults$run.txt/" BimolecularTest.txt
##More or less the same as above, except for line 31.
sed -i '' "31s/.*/cmd i 0 1 0.1 molcountspecies c bimolresults$run.txt/" BimolecularTest.txt
##Run Smoldyn with the edited configuration file
smoldyn BimolecularTest.txt
##Move onto the next run
run=$[$run+1]
done
