## This is a simple SGE batch script
## Use this file for jobs running Smoldyn

## Comment lines start with '##'

## Request Bash shell as shell for job
#!/bin/bash

## Define name of job
#$ -N smoyea_tvd

## Choose queue from {short,long,...}. The job will be run under this queue.
#$ -q long

## Send job status email to ... 
#$ -M ederzavala@me.com

## Send emails when job aborts(a), begins(b), ends(e) 
#$ -m a

## Standard error stream is merged into standard output stream
#$ -j yes


. $HOME/.bashrc

## Smoldyn files
SMOLFILE1=smoldynLA_src_tvd.txt
SMOLFILE2=LADumbbellIn.txt
SMOLFILE3=LADumbbellOut.txt
SMOLFILE4=LABleach.txt

## If the Smoldyn  program  creates output files define them here: 
SMOLFILEOUT1=outputLAsrcM.txt
SMOLFILEOUT2=outputLAsrcD.txt

HOMEDIR=/home/e/eder-zavala/work/smoldyn
WORKDIR=/work/MarquezU/eder/$JOB_NAME-$JOB_ID
SCRATCHDIR=/scratch/eder/$JOB_NAME-$JOB_ID

mkdir -p $WORKDIR
mkdir -p $SCRATCHDIR
cd $SCRATCHDIR

cp $HOMEDIR/$SMOLFILE1 .
cp $HOMEDIR/$SMOLFILE2 .
cp $HOMEDIR/$SMOLFILE3 .
cp $HOMEDIR/$SMOLFILE4 .

## Start smoldyn. Define barrier permeability and diffusion coefficients
/apps/marquez_unit/installed/smoldyn-2.28/bin/smoldyn $SMOLFILE1 -wt --define PLA=0.0005 --define DIFCOEF_A=0.4 --define DIFCOEF_B=0.3 --define DIFCOEF_C=0.2

## If Smoldyn creates any output files in the scratch folder
## copy them now to your work folder

cp $SMOLFILEOUT1 $WORKDIR/$SMOLFILEOUT1
cp $SMOLFILEOUT2 $WORKDIR/$SMOLFILEOUT2

rm -f $SCRATCHDIR -r
