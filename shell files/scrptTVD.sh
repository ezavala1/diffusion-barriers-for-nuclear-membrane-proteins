
## This is a simple Bourne shell script for
## submitting a series of jobs to tombo 
## Note: comment lines start with '#' with some exceptions; 
## e.g. '#!' is NOT a comment. I use '##' to avoid any confusions. 

## Request Bourne shell
#!/bin/sh

## loop from i=1 to N
for i in {1..34}
do
 ## use 'echo'to print argument 
 echo "Simulating cell $i..."
 ## clear existing configuration files in /work/smoldyn
 rm *.txt
 ## copy configuration files and geometries from i-th cell to working directory
 cp ./LA/cell_$i/*.txt .
 ## use qsub <JOBSCRIPT> to submit a job to the SGE (Sun Grid Engine)  
 qsub sge_script_SMOLDYN.sh
 ## use 'sleep n' to wait a few seconds between job submissions
 ## (necessary to avoid identical random seeds in 
 ## stochastic simulations) 
 sleep 10
done