
## This is a simple Bourne shell script for
## submitting a series of jobs to tombo 
## Note: comment lines start with '#' with some exceptions; 
## e.g. '#!' is NOT a comment. I use '##' to avoid any confusions. 

## Request Bourne shell
#!/bin/sh

## loop from i=1 to N
for i in {1..21}
do
 ## use 'echo'to print argument 
 echo "Simulating cell $i..."
 ## go to where i-th slave shell file is contained
 cd cell_$i
 ## use qsub <JOBSCRIPT> to submit a job to the SGE (Sun Grid Engine)  
 qsub slave_cell_$i.sh
 ## go to parent directory
 cd ..
 ## use 'sleep n' to wait a few seconds between job submissions
 sleep 10
done

echo "Wait at least 2 minutes before submitting next batch of jobs"