## This is a simple SGE batch script
## Use this file for jobs running Smoldyn

## Comment lines start with '##'

## Request Bash shell as shell for job
#!/bin/bash

## Define name of job
#$ -N smoyeaEA

## Choose queue from {short,long,...}. The job will be run under this queue.
#$ -q long

## Send job status email to ... 
#$ -M ederzavala@me.com

## Send emails when job aborts(a), begins(b), ends(e) 
#$ -m a

## Standard error stream is merged into standard output stream
#$ -j yes


. $HOME/.bashrc

## Smoldyn files
SMOLFILE1=smoldynEA.txt
SMOLFILE2=EADumbbellIn.txt
SMOLFILE3=EADumbbellOut.txt
SMOLFILE4=EABleach.txt
SMOLFILE5=cell_coord.txt

## If the Smoldyn  program  creates output files define them here: 
SMOLFILEOUT1=outputEA_M.txt
SMOLFILEOUT2=outputEA_D.txt

CONFIGDIR=/home/e/eder-zavala/work/smoldyn/geom-EA
GEOMETDIR=/home/e/eder-zavala/work/smoldyn/geom-EA/cell_18
WORKDIR=/work/MarquezU/eder/$JOB_NAME-$JOB_ID
SCRATCHDIR=/scratch/eder/$JOB_NAME-$JOB_ID

mkdir -p $WORKDIR
mkdir -p $SCRATCHDIR
cd $SCRATCHDIR

cp $CONFIGDIR/$SMOLFILE1 .
cp $GEOMETDIR/$SMOLFILE2 .
cp $GEOMETDIR/$SMOLFILE3 .
cp $GEOMETDIR/$SMOLFILE4 .
cp $GEOMETDIR/$SMOLFILE5 .

## Add a random pause to guarantee a random seed for Smoldyn
sleep $(($RANDOM % 100 + 10))

## Start smoldyn. May include additional definitions that override the ones specified in the config file
/apps/marquez_unit/installed/smoldyn-2.28/bin/smoldyn $SMOLFILE1 -wt

## If Smoldyn creates any output files in the scratch folder
## copy them now to your work folder

cp $SMOLFILEOUT1 $WORKDIR/$SMOLFILEOUT1
cp $SMOLFILEOUT2 $WORKDIR/$SMOLFILEOUT2

rm -f $SCRATCHDIR -r
