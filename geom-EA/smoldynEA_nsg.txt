# SMOLDYN 2.28 file
# Simple bouncing molecules with surfaces for testing yeast dumbbell nucleus in EARLY anaphase
# This file requires surface-defining files EABleach.txt, EADumbbellIn.txt and EADumbbellOut.txt in same folder
# (see Mathematica file yeastgeomEA.nb for source wrl files / see Python script wrltotxtEA.py for generating surfaces)
# --------------------------------------------------------------------------------------------------------------------

############ Global definitions ############

define TSTEP 0.00004   #Spatial-resolution = 7 nm for Diffusion rates up to 0.3 u^2/s in 2D (p17 Smoldyn manual)

define TSTART 0
define TSTOP 200      #Equals time-span of FLIP experiments + T_THERM
define TSAMPLING 2
define T_THERM 50       #Thermalization time needed for the molecules to 'settle'

define VOLEXC 0.008

define DIFCOEF 0.3

define PERM 0.0004    #Barrier permeability

############ Initial settings ############

# All units in microns and seconds

graphics opengl_good
graphic_iter 1
#cmd b pause           #comment this line if using cluster

############ System settings (space and time) ############

dim 3

# Frame must be big enough to include both lobes, if it doesn't, increase boundaries
boundaries 0 -5 5 
boundaries 1 -2 2
boundaries 2 -2 2
frame_thickness 1
grid_thickness 0

time_start TSTART
time_stop TSTOP      #Equals time-span of FLIP experiments + T_THERM
time_step TSTEP

############ Molecules ############

species nsg bleached temp

difc all(all) DIFCOEF

color nsg(all) teal
color bleached(all) orange

display_size all(front) VOLEXC

text_display time nsg(front) bleached(front)

############ Surfaces ############

start_surface EADumbbellOut
color both 0.5 0.5 0.5
polygon both edge
read_file EADumbbellOut.txt
end_surface

start_surface EABleach
rate_internal nsg(front) bsoln fsoln 1 temp
rate_internal temp(front) fsoln bsoln 1 nsg
color both 0.5 0.5 0.5
polygon both edge
read_file EABleach.txt
end_surface

start_surface septum
rate_internal all(front) bsoln fsoln PERM
rate_internal all(front) fsoln bsoln PERM
color both 0 0 1
polygon both edge
panel disk 0 0 0 0.7 -1 0 0 10
end_surface

surface_mol 1900 nsg(front) EADumbbellOut all all

############ Reactions ############

# Bleaching:
cmd @ T_THERM set reaction_surface EADumbbellOut bleaching temp(front) -> bleached(front) 150

############ Volume exclusion ############

#reaction bounce1 src(front) + src(front) -> src(front) + src(front)
#reaction bounce2 bleached(front) + bleached(front) -> bleached(front) + bleached(front)
#reaction bounce3 src(front) + bleached(front) -> src(front) + bleached(front)
#binding_radius bounce1 VOLEXC
#binding_radius bounce2 VOLEXC
#binding_radius bounce3 VOLEXC
#product_placement bounce1 bounce VOLEXC
#product_placement bounce2 bounce VOLEXC
#product_placement bounce3 bounce VOLEXC

############ Output ############

output_files outputEA_M.txt outputEA_D.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox -5 0 -2 2 -2 2 outputEA_M.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox 0 5 -2 2 -2 2 outputEA_D.txt

end_file
