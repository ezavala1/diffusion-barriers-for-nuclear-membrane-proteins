# SMOLDYN 2.26 definition file

# EARLY anaphase

# Cell 11
# --------------------------------------------------------------------------------------------------------------------

############ Compartment-defining coordinates ############

# All units in microns and seconds

define MCP -0.1
define DCP 0.1
define BCPx -1.35
define BCPy -0.7

compartment motherOut point MCP 0 0
compartment daughterOut point DCP 0 0
compartment motherIn point MCP 0 0
compartment daughterIn point DCP 0 0
compartment bleachspot point BCPx BCPy 0

end_file
