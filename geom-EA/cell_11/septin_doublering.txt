# SMOLDYN 2.26 definition file

# EARLY anaphase

# Cell 11
# --------------------------------------------------------------------------------------------------------------------

############ Protein ring surface ############

# All units in microns and seconds

define L 0.307
define R 0.3015

panel cyl -0.0045 0 0 -0.004 0 0 L 100 1
panel cyl 0.004 0 0 0.0045 0 0 R 100 1

end_file
