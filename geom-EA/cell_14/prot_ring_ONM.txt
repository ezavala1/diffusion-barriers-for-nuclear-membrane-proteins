# SMOLDYN 2.26 definition file

# EARLY anaphase

# Cell 14
# --------------------------------------------------------------------------------------------------------------------

############ Protein ring surface ############

# All units in microns and seconds

define R 0.342

panel cyl -0.0005 0 0 0.0005 0 0 R 100 1

end_file
