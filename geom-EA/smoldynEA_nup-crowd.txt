# SMOLDYN 2.28 file
# Simple bouncing molecules with surfaces for testing yeast dumbbell nucleus in EARLY anaphase
# This file requires surface-defining files EABleach.txt, EADumbbellIn.txt and EADumbbellOut.txt in same folder
# (see Mathematica file yeastgeomEA.nb for source wrl files / see Python script wrltotxtEA.py for generating surfaces)
# --------------------------------------------------------------------------------------------------------------------

############ Global definitions ############

define TSTEP 0.00004   #Spatial-resolution = 7 nm for Diffusion rates up to 0.3 u^2/s (p17 Smoldyn manual)

define TSTART 0
define TSTOP 200      #Equals time-span of FLIP experiments + T_THERM
define TSAMPLING 2

define RADII_NUP 0.049
define RADII_C 0.005

define DIFCOEF_NUP 0.2
define DIFCOEF_C 1

define PERM_NUP 0.00022    #Barrier permeability Nup49

############ Initial settings ############

# All units in microns and seconds

graphics opengl_good
graphic_iter 1
#cmd b pause           #comment this line if using cluster

############ System settings (space and time) ############

dim 3

# Frame must be big enough to include both lobes, if it doesn't, increase boundaries
boundaries 0 -5 5 
boundaries 1 -2 2
boundaries 2 -2 2
frame_thickness 1
grid_thickness 0

time_start TSTART
time_stop TSTOP      #Equals time-span of FLIP experiments + T_THERM
time_step TSTEP

############ Molecules ############

species nup bleached_nup crowder

difc nup DIFCOEF_NUP
difc bleached_nup DIFCOEF_NUP
difc crowder DIFCOEF_C

color nup purple
color bleached_nup orange
color crowder red

display_size nup RADII_NUP
display_size bleached_nup RADII_NUP
display_size crowder RADII_C

text_display time nup

############ Surfaces ############

start_surface EADumbbellOut
#action nup both reflect
#action bleached_nup both reflect
action all both reflect
color both 0.5 0.5 0.5
polygon both edge
read_file EADumbbellOut.txt
end_surface

start_surface EADumbbellIn
#action nup both reflect
#action bleached_nup both reflect
action all both reflect
color both 0.5 0.8 0.8
polygon both edge
read_file EADumbbellIn.txt
end_surface

start_surface EABleach
action all both transmit
color both 0.8 0.8 0.5
polygon both edge
read_file EABleach.txt
end_surface

start_surface septum
rate_internal nup bsoln fsoln PERM_NUP
rate_internal nup fsoln bsoln PERM_NUP
rate_internal bleached_nup bsoln fsoln PERM_NUP
rate_internal bleached_nup fsoln bsoln PERM_NUP
color both 0 0 1
polygon both edge
panel disk 0 0 0 0.7 -1 0 0 10
end_surface

############ Compartments ############

start_compartment motherOut
surface EADumbbellOut
surface septum
end_compartment

start_compartment daughterOut
surface EADumbbellOut
surface septum
end_compartment

start_compartment motherIn
surface EADumbbellIn
surface septum
end_compartment

start_compartment daughterIn
surface EADumbbellIn
surface septum
end_compartment

start_compartment bleachspot
surface EABleach
end_compartment

read_file cell_coord.txt   #file containing compartment defining spatial coordinates

start_compartment mother_periplasm
compartment equal motherOut
compartment andnot motherIn
end_compartment

start_compartment daughter_periplasm
compartment equal daughterOut
compartment andnot daughterIn
end_compartment

start_compartment periplasm
compartment equal mother_periplasm
compartment or daughter_periplasm
end_compartment

############ Initial conditions ############

compartment_mol 150 nup periplasm
compartment_mol 5000 crowder periplasm

############ Reactions ############

# Bleaching:
reaction_cmpt bleachspot bleaching_nup nup -> bleached_nup 150

############ Volume exclusion ############

# Reactions:
reaction bounce01 nup + nup -> nup + nup

reaction bounce06 bleached_nup + bleached_nup -> bleached_nup + bleached_nup

reaction bounce11 nup + bleached_nup -> nup + bleached_nup

reaction bounceC1 nup + crowder -> nup + crowder

reaction bounceC2 bleached_nup + crowder -> bleached_nup + crowder

# Center-to-center distance for pair collisions
binding_radius bounce01 0.098
binding_radius bounce06 0.098
binding_radius bounce11 0.098
binding_radius bounceC1 0.103
binding_radius bounceC2 0.103

# Product placement after collision (equals binding_radius +1 nm)
product_placement bounce01 bounce 0.099
product_placement bounce06 bounce 0.099
product_placement bounce11 bounce 0.099
product_placement bounceC1 bounce 0.104
product_placement bounceC2 bounce 0.104

############ Movie ############

#tiff_iter 100000    # Choose 1e5 for 1 tiff frame per second in a 200 seconds simulation (aprox 5 MB per tiff file)

############ Output ############

output_files outputEA_M.txt outputEA_D.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox -5 0 -2 2 -2 2 outputEA_M.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox 0 5 -2 2 -2 2 outputEA_D.txt

end_file
