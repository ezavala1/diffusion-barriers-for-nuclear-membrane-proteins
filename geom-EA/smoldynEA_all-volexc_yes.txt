# SMOLDYN 2.28 file
# Simple bouncing molecules with surfaces for testing yeast dumbbell nucleus in EARLY anaphase
# This file requires surface-defining files EABleach.txt, EADumbbellIn.txt and EADumbbellOut.txt in same folder
# (see Mathematica file yeastgeomEA.nb for source wrl files / see Python script wrltotxtEA.py for generating surfaces)
# --------------------------------------------------------------------------------------------------------------------

############ Global definitions ############

define TSTEP 0.00004   #Spatial-resolution = 7 nm for Diffusion rates up to 0.3 u^2/s (p17 Smoldyn manual)

define TSTART 0
define TSTOP 200      #Equals time-span of FLIP experiments + T_THERM
define TSAMPLING 2

define T_THERM 50       #Thermalization time needed for the molecules to 'settle'

define RADII_NUP 0.049
define RADII_NSG 0.004
define RADII_SRC 0.005

define DIFCOEF_NUP 0.2
define DIFCOEF_NSG 0.3
define DIFCOEF_SRC 0.3

define PERM_NUP 0.00022    #Barrier permeability Nup49
define PERM_NSG 0.0004    #Barrier permeability Nsg1
define PERM_SRC 0.0015    #Barrier permeability Src1

############ Initial settings ############

# All units in microns and seconds

graphics opengl_good
graphic_iter 1
#cmd b pause           #comment this line if using cluster

############ System settings (space and time) ############

dim 3

# Frame must be big enough to include both lobes, if it doesn't, increase boundaries
boundaries 0 -5 5 
boundaries 1 -2 2
boundaries 2 -2 2
frame_thickness 1
grid_thickness 0

time_start TSTART
time_stop TSTOP      #Equals time-span of FLIP experiments + T_THERM
time_step TSTEP

############ Molecules ############

species nup nsg src bleached_nup bleached_nsg bleached_src temp_nsg temp_src

difc nup DIFCOEF_NUP
difc nsg(front) DIFCOEF_NSG
difc src(front) DIFCOEF_SRC
difc bleached_nup DIFCOEF_NUP
difc bleached_nsg(front) DIFCOEF_NSG
difc bleached_src(front) DIFCOEF_SRC
difc temp_nsg(front) DIFCOEF_NSG
difc temp_src(front) DIFCOEF_SRC

color nup purple
color nsg(front) teal
color src(front) orange
color bleached_nup red
color bleached_nsg(front) red
color bleached_src(front) red

display_size nup RADII_NUP
display_size nsg(front) RADII_NSG
display_size src(front) RADII_SRC
display_size bleached_nup RADII_NUP
display_size bleached_nsg(front) RADII_NSG
display_size bleached_src(front) RADII_SRC
display_size temp_nsg(front) RADII_NSG
display_size temp_src(front) RADII_SRC

text_display time nup nsg(front) src(front)

############ Surfaces ############

start_surface EADumbbellOut
action nup both reflect
action bleached_nup both reflect
color both 0.5 0.5 0.5
polygon both edge
read_file EADumbbellOut.txt
end_surface

start_surface EADumbbellIn
action nup both reflect
action bleached_nup both reflect
color both 0.5 0.8 0.8
polygon both edge
read_file EADumbbellIn.txt
end_surface

start_surface EABleach
rate_internal nsg(front) bsoln fsoln 1 temp_nsg
rate_internal temp_nsg(front) fsoln bsoln 1 nsg
rate_internal src(front) bsoln fsoln 1 temp_src
rate_internal temp_src(front) fsoln bsoln 1 src
color both 0.8 0.8 0.5
polygon both edge
read_file EABleach.txt
end_surface

start_surface septum
rate_internal nup bsoln fsoln PERM_NUP
rate_internal nup fsoln bsoln PERM_NUP
rate_internal bleached_nup bsoln fsoln PERM_NUP
rate_internal bleached_nup fsoln bsoln PERM_NUP
rate_internal nsg(front) bsoln fsoln PERM_NSG
rate_internal nsg(front) fsoln bsoln PERM_NSG
rate_internal bleached_nsg(front) bsoln fsoln PERM_NSG
rate_internal bleached_nsg(front) fsoln bsoln PERM_NSG
rate_internal src(front) bsoln fsoln PERM_SRC
rate_internal src(front) fsoln bsoln PERM_SRC
rate_internal bleached_src(front) bsoln fsoln PERM_SRC
rate_internal bleached_src(front) fsoln bsoln PERM_SRC
color both 0 0 1
polygon both edge
panel disk 0 0 0 0.7 -1 0 0 10
end_surface

############ Compartments ############

start_compartment motherOut
surface EADumbbellOut
surface septum
end_compartment

start_compartment daughterOut
surface EADumbbellOut
surface septum
end_compartment

start_compartment motherIn
surface EADumbbellIn
surface septum
end_compartment

start_compartment daughterIn
surface EADumbbellIn
surface septum
end_compartment

start_compartment bleachspot
surface EABleach
end_compartment

read_file cell_coord.txt   #file containing compartment defining spatial coordinates

start_compartment mother_periplasm
compartment equal motherOut
compartment andnot motherIn
end_compartment

start_compartment daughter_periplasm
compartment equal daughterOut
compartment andnot daughterIn
end_compartment

start_compartment periplasm
compartment equal mother_periplasm
compartment or daughter_periplasm
end_compartment

############ Initial conditions ############

compartment_mol 150 nup periplasm
surface_mol 1900 nsg(front) EADumbbellOut all all
surface_mol 2100 src(front) EADumbbellIn all all

############ Reactions ############

# Bleaching:
cmd @ T_THERM set reaction_cmpt bleachspot bleaching_nup nup -> bleached_nup 150
cmd @ T_THERM set reaction_cmpt bleachspot bleaching_nsg temp_nsg(front) -> bleached_nsg(front) 150
cmd @ T_THERM set reaction_cmpt bleachspot bleaching_src temp_src(front) -> bleached_src(front) 150

############ Volume exclusion ############

# Note: Perinuclear space thickness is 13 nm at its minimum, hence Nsg1-GFP (ONM) and Src1-GFP (INM) never collide.

# Reactions:
reaction bounce01 nup + nup -> nup + nup
reaction bounce02 nup + nsg(front) -> nup + nsg(front)
reaction bounce03 nup + src(front) -> nup + src(front)
reaction bounce04 nsg(front) + nsg(front) -> nsg(front) + nsg(front)
reaction bounce05 src(front) + src(front) -> src(front) + src(front)

reaction bounce06 bleached_nup + bleached_nup -> bleached_nup + bleached_nup
reaction bounce07 bleached_nup + bleached_nsg(front) -> bleached_nup + bleached_nsg(front)
reaction bounce08 bleached_nup + bleached_src(front) -> bleached_nup + bleached_src(front)
reaction bounce09 bleached_nsg(front) + bleached_nsg(front) -> bleached_nsg(front) + bleached_nsg(front)
reaction bounce10 bleached_src(front) + bleached_src(front) -> bleached_src(front) + bleached_src(front)

reaction bounce11 nup + bleached_nup -> nup + bleached_nup
reaction bounce12 nup + bleached_nsg(front) -> nup + bleached_nsg(front)
reaction bounce13 nup + bleached_src(front) -> nup + bleached_src(front)
reaction bounce14 nsg(front) + bleached_nup -> nsg(front) + bleached_nup
reaction bounce15 nsg(front) + bleached_nsg(front) -> nsg(front) + bleached_nsg(front)
reaction bounce16 src(front) + bleached_nup -> src(front) + bleached_nup
reaction bounce17 src(front) + bleached_src(front) -> src(front) + bleached_src(front)

reaction bounce18 nup + temp_nsg(front) -> nup + temp_nsg(front)
reaction bounce19 nup + temp_src(front) -> nup + temp_src(front)
reaction bounce20 nsg(front) + temp_nsg(front) -> nsg(front) + temp_nsg(front)
reaction bounce21 src(front) + temp_src(front) -> src(front) + temp_src(front)

reaction bounce22 bleached_nup + temp_nsg(front) -> bleached_nup + temp_nsg(front)
reaction bounce23 bleached_nup + temp_src(front) -> bleached_nup + temp_src(front)
reaction bounce24 bleached_nsg(front) + temp_nsg(front) -> bleached_nsg(front) + temp_nsg(front)
reaction bounce25 bleached_src(front) + temp_src(front) -> bleached_src(front) + temp_src(front)

reaction bounce26 temp_nsg(front) + temp_nsg(front) -> temp_nsg(front) + temp_nsg(front)
reaction bounce27 temp_src(front) + temp_src(front) -> temp_src(front) + temp_src(front)

# Center-to-center distance for pair collisions
binding_radius bounce01 0.098
binding_radius bounce02 0.053
binding_radius bounce03 0.054
binding_radius bounce04 0.008
binding_radius bounce05 0.010
binding_radius bounce06 0.098
binding_radius bounce07 0.053
binding_radius bounce08 0.054
binding_radius bounce09 0.008
binding_radius bounce10 0.010
binding_radius bounce11 0.098
binding_radius bounce12 0.053
binding_radius bounce13 0.054
binding_radius bounce14 0.053
binding_radius bounce15 0.008
binding_radius bounce16 0.054
binding_radius bounce17 0.010
binding_radius bounce18 0.053
binding_radius bounce19 0.054
binding_radius bounce20 0.008
binding_radius bounce21 0.010
binding_radius bounce22 0.053
binding_radius bounce23 0.054
binding_radius bounce24 0.008
binding_radius bounce25 0.010
binding_radius bounce26 0.008
binding_radius bounce27 0.010

# Product placement after collision (equals binding_radius +1 nm)
product_placement bounce01 bounce 0.099
product_placement bounce02 bounce 0.054
product_placement bounce03 bounce 0.055
product_placement bounce04 bounce 0.009
product_placement bounce05 bounce 0.011
product_placement bounce06 bounce 0.099
product_placement bounce07 bounce 0.054
product_placement bounce08 bounce 0.055
product_placement bounce09 bounce 0.009
product_placement bounce10 bounce 0.011
product_placement bounce11 bounce 0.099
product_placement bounce12 bounce 0.054
product_placement bounce13 bounce 0.055
product_placement bounce14 bounce 0.054
product_placement bounce15 bounce 0.009
product_placement bounce16 bounce 0.055
product_placement bounce17 bounce 0.011
product_placement bounce18 bounce 0.054
product_placement bounce19 bounce 0.055
product_placement bounce20 bounce 0.009
product_placement bounce21 bounce 0.011
product_placement bounce22 bounce 0.054
product_placement bounce23 bounce 0.055
product_placement bounce24 bounce 0.009
product_placement bounce25 bounce 0.011
product_placement bounce26 bounce 0.009
product_placement bounce27 bounce 0.011

############ Movie ############

#tiff_iter 100000    # Choose 1e5 for 1 tiff frame per second in a 200 seconds simulation (aprox 5 MB per tiff file)

############ Output ############

output_files outputEA_M.txt outputEA_D.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox -5 0 -2 2 -2 2 outputEA_M.txt
cmd i TSTART TSTOP TSAMPLING molcountinbox 0 5 -2 2 -2 2 outputEA_D.txt

end_file
