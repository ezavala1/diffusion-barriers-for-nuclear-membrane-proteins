%% Script for importing and processing output from Yeast nuclei bleaching experiments and compare against stochastic simulations for estimation of the bleaching rate

clear

% Set path to where output .txt files are contained. Use 'zavalaeder' if using MacPro at OIST
% addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/smoldynJCB/output_JCB');
% listing = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/smoldynJCB/output_JCB/*.txt');
addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_150');
listing = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_150/*.txt');


% Import data from files
n=size(listing,1);
data=[];
for i = 1:n
  data = cat(3,data,importdata(listing(i,1).name,' '));
end

% Time information
time = mean(data(:,1),3);

% Scale to 100% fluorescence equivalency
mother=[];
daughter=[];
for j = 1:n
    mother = cat(2,mother,data(:,2,j)*100/data(1,2,j));
  daughter = cat(2,daughter,data(:,4,j)*100/data(1,4,j));
end

% Calculate statistics
  mother_mean = mean(mother,2);
daughter_mean = mean(daughter,2);
   mother_err = std(mother,0,2);
 daughter_err = std(daughter,0,2);

% Load data from measurements reported on JCB to compare against
% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Yeast/expdata/Exp data JCB - Matlab format/');
TetR = importdata('Early_TetR_experimental.mat');

% Plot mother lobe
figure(1); clf;
set(gcf,'Color','white');
errorbar(TetR(:,1),TetR(:,3),TetR(:,4),'-x','Color','black');  % experimental data
hold on
errorbar(time,mother_mean,mother_err,'-o','Color','red');  % stochastic simulation
axis([0 100 0 100]);
title({'\fontsize{14}TetR-GFP in Early Anaphase';['\fontsize{12}Mother lobe / n_{simulations} = ',num2str(n)]});
xlabel('\fontsize{12}time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend('FLIP exp','k_{deg} = 150 s^{-1}');

% Plot daughter lobe
figure(2); clf;
set(gcf,'Color','white');
errorbar(TetR(:,1),TetR(:,5),TetR(:,6),'-x','Color','black');  % experimental data
hold on
errorbar(time,daughter_mean,daughter_err,'-o','Color',[0 0.5 0]);  % stochastic simulation
axis([0 100 0 100]);
title({'\fontsize{14}TetR-GFP in Early Anaphase';['\fontsize{12}Daughter lobe / n_{simulations} = ',num2str(n)]});
xlabel('\fontsize{12}time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend('FLIP exp','k_{deg} = 150 s^{-1}');

