# SMOLDYN 2.26 file
# Simple bouncing molecules with surfaces for testing yeast dumbbell nucleus in EARLY anaphase
# This file requires surface-defining files EADumbbellIn.txt and EADumbbellOut.txt in same folder
# (see JCB paper for generating surfaces)
# --------------------------------------------------------------------------------------------------------------------

############ Initial settings ############

# All units in microns and seconds

graphics opengl_good
graphic_iter 1
#cmd b pause           # comment this line if using cluster

define TSTART 0
define TSTOP 100
define TSTEP 0.0001
define TSAMPLING 2

define DIFCOEF 1.9
define VOLEXC 0.008

define MCP -0.1
define DCP 0.1
define BCPx -1.5595
define BCPy -0.731

############ System settings (space and time) ############

dim 3

# Frame must be big enough to comprise both lobes, if it doesn't, increase boundaries
boundaries 0 -5 5 
boundaries 1 -2 2
boundaries 2 -2 2
frame_thickness 1
grid_thickness 0
#boxsize 1

time_start TSTART
time_stop TSTOP
time_step TSTEP

############ Molecules ############

species tetR bleached 
difc tetR DIFCOEF
difc bleached DIFCOEF
color tetR teal
color bleached orange

# Uncomment to 'see' point defining bleachspot compartment
#species x_bleachspot
#mol 1 x_bleachspot BCPx BCPy 0
#difc x_bleachspot 0
#color x_bleachspot lime

display_size all VOLEXC

text_display time tetR bleached

############ Surfaces ############

start_surface EADumbbellOut
action all both reflect
color both 0.5 0.5 0.5
polygon both edge
read_file EADumbbellOut.txt
end_surface

start_surface EADumbbellIn
action all both reflect
color both 0.5 0.5 0.5
polygon both edge
read_file EADumbbellIn.txt
end_surface

start_surface EABleach
action all both transmit
color both 0.5 0.5 0.5
polygon both edge
panel sph BCPx BCPy 0 0.1651 10 10
end_surface

start_surface septum
action all both transmit
rate_internal all fsoln bsoln 1
rate_internal all bsoln fsoln 1
color both 0 0 1
polygon both edge
panel disk 0 0 0 0.7 1 0 0 10
end_surface

############ Compartments ############

start_compartment mother
surface EADumbbellIn
surface septum
point MCP 0 0
end_compartment

start_compartment daughter
surface EADumbbellIn
surface septum
point DCP 0 0
end_compartment

start_compartment nucleoplasm
compartment equal mother
compartment or daughter
end_compartment

start_compartment bleachspot
surface EABleach
point BCPx BCPy 0
end_compartment

compartment_mol 5000 tetR nucleoplasm

############ Reactions ############

# Bleaching:
reaction_cmpt bleachspot bleaching tetR -> 0 110

# Volume exclusion:
#reaction bounce1 tetR + tetR -> tetR + tetR
#reaction bounce2 bleached + bleached -> bleached + bleached
#reaction bounce3 tetR + bleached -> tetR + bleached
#binding_radius bounce1 VOLEXC
#binding_radius bounce2 VOLEXC
#binding_radius bounce3 VOLEXC
#product_placement bounce1 bounce VOLEXC
#product_placement bounce2 bounce VOLEXC
#product_placement bounce3 bounce VOLEXC

############ Output ############

output_files outputEAtetR.txt
cmd i TSTART TSTOP TSAMPLING molcountincmpts mother daughter outputEAtetR.txt

end_file
