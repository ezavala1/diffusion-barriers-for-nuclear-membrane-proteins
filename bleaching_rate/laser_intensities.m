%% Script for importing and processing output from Yeast nuclei bleaching virtual and real experiments

clear

%% Retrieve experimental data of bleaching at 60, 30 and 15% laser intensities

% Set path to where output .txt files are contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Yeast/expdata/');

% Import XLSX file specifying sheet for each bleaching intensity
I_60 = xlsread('/Users/zavalaeder/Dropbox/Yeast/expdata/120920_different bleaching intensities.xlsx','60%','B:D');
I_30 = xlsread('/Users/zavalaeder/Dropbox/Yeast/expdata/120920_different bleaching intensities.xlsx','30%','B:D');
I_15 = xlsread('/Users/zavalaeder/Dropbox/Yeast/expdata/120920_different bleaching intensities.xlsx','15%','B:D');

% Calculate statistics
m_60=[]; m_30=[]; m_15=[];
s_60=[]; s_30=[]; s_15=[];
for i = 1:60
   m_60 = [m_60;mean(I_60(1+(i-1)*10:10+(i-1)*10,:))];
   m_30 = [m_30;mean(I_30(1+(i-1)*10:10+(i-1)*10,:))];
   m_15 = [m_15;mean(I_15(1+(i-1)*10:10+(i-1)*10,:))];
   s_60 = [s_60;std(I_60(1+(i-1)*10:10+(i-1)*10,:))];
   s_30 = [s_30;std(I_60(1+(i-1)*10:10+(i-1)*10,:))];
   s_15 = [s_15;std(I_60(1+(i-1)*10:10+(i-1)*10,:))];
end

% Time vectors (mean and std)
time_60 = [m_60(:,1),s_60(:,1)];
time_30 = [m_30(:,1),s_30(:,1)];
time_15 = [m_15(:,1),s_15(:,1)];

% Mother data (mean and std)
mother_60 = [m_60(:,2),s_60(:,2)];
mother_30 = [m_30(:,2),s_30(:,2)];
mother_15 = [m_15(:,2),s_15(:,2)];

% Daughter data (mean and std)
daughter_60 = [m_60(:,3),s_60(:,3)];
daughter_30 = [m_30(:,3),s_30(:,3)];
daughter_15 = [m_15(:,3),s_15(:,3)];

% Plot experimental data with error bars
figure(1); clf;
set(gcf,'Color','white');
errorbar(time_60(:,1),mother_60(:,1),mother_60(:,2),'-o','Color',[1 0 0 ]);
hold on
errorbar(time_30(:,1),mother_30(:,1),mother_30(:,2),'-o','Color',[1 0.4 0]);
hold on
errorbar(time_15(:,1),mother_15(:,1),mother_15(:,2),'-o','Color',[1 0.7 0]);
axis([0 100 0 100]);
title({'\fontsize{14}Fluorescence decay at different bleaching intensities / rates';'\fontsize{12}TetR - mother lobe'});
xlabel('\fontsize{12}time (s)');
ylabel('\fontsize{12}% Fluorescence');

figure(2); clf;
set(gcf,'Color','white');
errorbar(time_60(:,1),daughter_60(:,1),daughter_60(:,2),'-o','Color',[0 0.5 0 ]);
hold on
errorbar(time_30(:,1),daughter_30(:,1),daughter_30(:,2),'-o','Color',[0 0.8 0]);
hold on
errorbar(time_15(:,1),daughter_15(:,1),daughter_15(:,2),'-o','Color',[0.7 1 0]);
axis([0 100 0 100]);
title({'\fontsize{14}Fluorescence decay at different bleaching intensities / rates';'\fontsize{12}TetR - daughter lobe'});
xlabel('\fontsize{12}time (s)');
ylabel('\fontsize{12}% Fluorescence');

%% Retrieve stochastic simulation data of bleaching at 110, 55 and 27.5 1/s bleaching rates

% Set path to where output .txt files are contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_110');
addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_55');
addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_27.5');
addpath('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_500');
listing_A = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_110/*.txt');
listing_B = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_55/*.txt');
listing_C = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_27.5/*.txt');
listing_D = dir('/Users/zavalaeder/Dropbox/Yeast/bleaching_rate/output_500/*.txt');

% Import data from files
n=100;
data_A=[];
data_B=[];
data_C=[];
data_D=[];
for i = 1:n
  data_A = cat(3,data_A,importdata(listing_A(i,1).name,' '));
  data_B = cat(3,data_B,importdata(listing_B(i,1).name,' '));
  data_C = cat(3,data_C,importdata(listing_C(i,1).name,' '));
  data_D = cat(3,data_D,importdata(listing_D(i,1).name,' '));
end

% Time information
time_A = mean(data_A(:,1),3);
time_B = mean(data_B(:,1),3);
time_C = mean(data_C(:,1),3);
time_D = mean(data_D(:,1),3);

% Scale to 100% fluorescence equivalency
mother_A=[];
mother_B=[];
mother_C=[];
mother_D=[];
daughter_A=[];
daughter_B=[];
daughter_C=[];
daughter_D=[];
for j = 1:n
    mother_A = cat(2,mother_A,data_A(:,2,j)*100/data_A(1,2,j));
    mother_B = cat(2,mother_B,data_B(:,2,j)*100/data_B(1,2,j));
    mother_C = cat(2,mother_C,data_C(:,2,j)*100/data_C(1,2,j));
    mother_D = cat(2,mother_D,data_D(:,2,j)*100/data_D(1,2,j));
  daughter_A = cat(2,daughter_A,data_A(:,4,j)*100/data_A(1,4,j));
  daughter_B = cat(2,daughter_B,data_B(:,4,j)*100/data_B(1,4,j));
  daughter_C = cat(2,daughter_C,data_C(:,4,j)*100/data_C(1,4,j));
  daughter_D = cat(2,daughter_D,data_D(:,4,j)*100/data_D(1,4,j));
end

% Calculate statistics
  mother_A_mean = mean(mother_A,2);
  mother_B_mean = mean(mother_B,2);
  mother_C_mean = mean(mother_C,2);
  mother_D_mean = mean(mother_D,2);
daughter_A_mean = mean(daughter_A,2);
daughter_B_mean = mean(daughter_B,2);
daughter_C_mean = mean(daughter_C,2);
daughter_D_mean = mean(daughter_D,2);
   mother_A_err = std(mother_A,0,2);
   mother_B_err = std(mother_B,0,2);
   mother_C_err = std(mother_C,0,2);
   mother_D_err = std(mother_D,0,2);
 daughter_A_err = std(daughter_A,0,2);
 daughter_B_err = std(daughter_B,0,2);
 daughter_C_err = std(daughter_C,0,2);
 daughter_D_err = std(daughter_D,0,2);

% Plot with error bars
figure(1);
errorbar(time_A,mother_A_mean,mother_A_err,'-','Color',[0 0 0]);
hold on
errorbar(time_B,mother_B_mean,mother_B_err,'-','Color',[0.5 0.5 0.5]);
hold on
errorbar(time_C,mother_C_mean,mother_C_err,'-','Color',[0.8 0.8 0.8]);
hold on
errorbar(time_D,mother_D_mean,mother_D_err,'-','Color',[0 0.8 1]);
legend('60%','30%','15%','110 s^{-1}','55 s^{-1}','27.5 s^{-1}','500 s^{-1}');
figure(2);
errorbar(time_A,daughter_A_mean,daughter_A_err,'-','Color',[0 0 0]);
hold on
errorbar(time_B,daughter_B_mean,daughter_B_err,'-','Color',[0.5 0.5 0.5]);
hold on
errorbar(time_C,daughter_C_mean,daughter_C_err,'-','Color',[0.8 0.8 0.8]);
hold on
errorbar(time_D,daughter_D_mean,daughter_D_err,'-','Color',[0 0.8 1]);
legend('60%','30%','15%','110 s^{-1}','55 s^{-1}','27.5 s^{-1}','500 s^{-1}');

%% Retrieve experimental data of bleaching at 40x and 20x magnification

% Import XLSX file specifying sheet for each magnification
mag40x = xlsread('/Users/zavalaeder/Dropbox/Yeast/expdata/av stdev_20x 40x.xlsx','40x','B:D');
mag20x = xlsread('/Users/zavalaeder/Dropbox/Yeast/expdata/av stdev_20x 40x.xlsx','20x','B:D');

% Calculate statistics
m_40x=[]; m_20x=[];
s_40x=[]; s_20x=[];
for i = 1:60
   m_40x = [m_40x;mean(mag40x(1+(i-1)*30:30+(i-1)*30,:))];
   m_20x = [m_20x;mean(mag20x(1+(i-1)*29:29+(i-1)*29,:))];
   s_40x = [s_40x;std(mag40x(1+(i-1)*30:30+(i-1)*30,:))];
   s_20x = [s_20x;std(mag20x(1+(i-1)*29:29+(i-1)*29,:))];
end

% Time vectors (mean and std)
time_40x = [m_40x(:,1),s_40x(:,1)];
time_20x = [m_20x(:,1),s_20x(:,1)];

% Mother data (mean and std)
mother_40x = [m_40x(:,2),s_40x(:,2)];
mother_20x = [m_20x(:,2),s_20x(:,2)];

% Daughter data (mean and std)
daughter_40x = [m_40x(:,3),s_40x(:,3)];
daughter_20x = [m_20x(:,3),s_20x(:,3)];

% Plot experimental data with error bars
figure(3); clf;
set(gcf,'Color','white');

% errorbar(time_40x(:,1),mother_40x(:,1),mother_40x(:,2),'-o','Color',[1 0 0]);
% hold on
% errorbar(time_20x(:,1),mother_20x(:,1),mother_20x(:,2),'-o','Color',[1 0.4 0]);
% hold on
% errorbar(time_40x(:,1),daughter_40x(:,1),daughter_40x(:,2),'-o','Color',[0 0.5 0]);
% hold on
% errorbar(time_20x(:,1),daughter_20x(:,1),daughter_20x(:,2),'-o','Color',[0 0.8 0]);

plot(time_40x(:,1),mother_40x(:,1),'o','Color',[1 0 0]);
hold on
plot(time_20x(:,1),mother_20x(:,1),'x','Color',[1 0.4 0]);
hold on
plot(time_40x(:,1),daughter_40x(:,1),'o','Color',[0 0.5 0]);
hold on
plot(time_20x(:,1),daughter_20x(:,1),'x','Color',[0 0.8 0]);

axis([0 70 0 100]);
title({'\fontsize{14}Fluorescence decay at different microscope magnifications';'\fontsize{12}TetR / 60% laser intensity'});
xlabel('\fontsize{12}time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend('mother 40x','mother 20x','daughter 40x','daughter 20x');


