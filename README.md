# Diffusion barriers for nuclear membrane proteins

Codes associated to the paper https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003725

Includes VRML delaunay triangulations of yeast nuclei, Mathematica notebook visualizations, Python and Shell scripts, and Smoldyn simulations.