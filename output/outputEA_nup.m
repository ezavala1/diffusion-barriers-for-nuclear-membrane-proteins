%% Script for importing and processing output from Yeast nuclei bleaching virtual experiments

clear

% Select folder containing .txt Smoldyn output files
% pfolder = 'd_0.2-p_0.00022-volexc_no-ts_0.00004';
% pfolder = 'd_0.2-p_0.00022-volexc_yes-ts_0.00004';
pfolder = 'crowding_highres_3000';

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/EA - Nup/';
addpath([ppath pfolder])

% Sort the mother and daughter lobe data files
pvarM = '/*M.txt';
pvarD = '/*D.txt';
listingM = dir([ppath pfolder pvarM]);
listingD = dir([ppath pfolder pvarD]);

% Import data from files
n=size(listingM,1);
dataM=[];
dataD=[];
for i = 1:n
  dataM = cat(3,dataM,importdata(listingM(i,1).name,' '));
  dataD = cat(3,dataD,importdata(listingD(i,1).name,' '));
end

% Time information
time = mean(dataM(:,1),3);

% Scale to 100% fluorescence equivalency
mother=[];
daughter=[];
tt=1;     % thermalization threshold, bleaching reaction starts at this time-point
for j = 1:n
    mother = cat(2,mother,dataM(:,2,j)*100/dataM(tt,2,j));
  daughter = cat(2,daughter,dataD(:,2,j)*100/dataD(tt,2,j));
end

% Calculate statistics
  mother_mean = mean(mother,2);
daughter_mean = mean(daughter,2);
   mother_err = std(mother,0,2);
 daughter_err = std(daughter,0,2);

% Load data from measurements reported on JCB to compare against
% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Project - Yeast/expdata/Exp data JCB - Matlab format/');
nup = importdata('Early_Nup49_experimental.mat');

% Plot FLIP experiments vs simulation with error bars
figure(3); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(nup(:,1),nup(:,3),nup(:,4),'-x','Color','black','LineWidth',0.5);  % mother lobe
hold on
errorbar(nup(:,1),nup(:,5),nup(:,6),'-x','Color','black','LineWidth',0.5);  % daughter lobe
hold on

% Simulation
h(1) = errorbar(time,mother_mean,mother_err,'-o','Color',[1 0 0],'LineWidth',0.5);
hold on
h(2) = errorbar(time,daughter_mean,daughter_err,'-o','Color',[0 0.5 0],'LineWidth',0.5);
hold on

set(gca,'FontSize',40);
box off

axis([0 200 0 100]);
% title({'\fontsize{14}Nup49-GFP in Early Anaphase';['\fontsize{12}n_{cells} = ',num2str(n),'    -    Permeability = 0.00022']});
% xlabel('\fontsize{12}Time (s)');
% ylabel('\fontsize{12}% Fluorescence');
% legend(h,'mother','daughter'); legend('boxoff');

text(1,0.25,'P = 0.022%','Units','normalized','HorizontalAlignment','right','FontSize',40)

%% Save Figure

% saveas(1,'/Users/zavalaeder/Dropbox/Project - Yeast/paper/figures/fig_S2/EA_Nup49-P_022','epsc2');
% saveas(1,'/Users/zavalaeder/Dropbox/Project - Yeast/paper/figures/fig_S3/EA_Nup49-P_022-volexc','epsc2');
