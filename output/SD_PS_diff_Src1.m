%%%%%%%%%%%%%%%%%%%%%%%%% Saffman-Delbruck model %%%%%%%%%%%%%%%%%%%%%%%%%

% The model describes the diffusion coefficient of a cylindrical inclusion
% of radius a in a membrane with thickness h and viscosity v_m, surrounded
% by bulk fluid with viscosity v_f

% SaffmanDelbruck 1975 PNAS 72(8) 3111-3113

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% All parameters in SI units

clear

format long

%% Global parameters

 kB = 1.3806e-23;  % Boltzmann constant
 em = 0.577215;      % Euler-Mascheroni constant
  T = 303.15;      % Temperature in at which FLIP experiments were carried out (30 C)
v_c = 11.967e-4;   % Bulk viscosity of cytoplasm in Pa s at 30 C
h_m = 4e-9;        % Membrane thickness
h_b = 5.4e-9;      % Domain thickness

%% Local parameters

Dm_Nsg1 = 0.3e-12;   % Effective diffusion coefficient at membrane for Nsg1
Dm_Src1 = 0.3e-12;   % Effective diffusion coefficient at membrane for Src1
 Dm_NPC = 0.2e-12;   % Effective diffusion coefficient at membrane for NPC

a_Nsg1 = 4e-9;   % Nsg1-GFP radii
a_Src1 = 5e-9;   % Src1-GFP radii
 a_NPC = 49e-9;  % NPC radii

%% Re-assignement of local variables

Dm = Dm_Src1;
a = a_Src1;

%% Saffman-Delbruck classical relation

% Intersecting functions
x = [1500*v_c:.001*v_c:1520*v_c];
Y1 = exp(x*(4*pi*h_m*Dm)/(kB*T));
Y2 = x*((h_m*exp(-em))/(v_c*a));

% Plot intersection
figure(1); clf;
plot(x,Y1,x,Y2);
axis([x(1) x(end) 670 690]);

yL = get(gca,'YLim');
line([v_c v_c],yL,'Color','r');

v_m_SD = x(find(Y1==Y2+min(abs(Y1-Y2))))

% Reduced radius (should follow eps << 1)
eps_SD = a*(2*v_c)/(v_m_SD*h_m)

%% Petrov-Schwille approximation

% Local parameters

c1 = 0.73761;
b1 = 2.74819;
c2 = 0.52119;
b2 = 0.61465;

% Intersecting functions
z = [1500*v_c:0.001*v_c:1520*v_c];
Y3 = z*(4*pi*h_m*Dm)/(kB*T);
Y4 = (log(2./(a*(2*v_c)./(z*h_m))) - em + 4*(a*(2*v_c)./(z*h_m))/pi - (((a*(2*v_c)./(z*h_m)).^2)/2).*log(2./(a*(2*v_c)./(z*h_m)))) ./ (1 - (((a*(2*v_c)./(z*h_m)).^3)/pi).*log(2./(a*(2*v_c)./(z*h_m))) + (c1*(a*(2*v_c)./(z*h_m)).^b1)/(1+c2*(a*(2*v_c)./(z*h_m)).^b2));

% Plot intersection
figure(2); clf;
plot(z,Y3,z,Y4);
axis([z(1) z(end) 6.46 6.56]);

yL = get(gca,'YLim');
line([v_c v_c],yL,'Color','r');

v_m_PS = z(find(Y4==Y3+min(abs(Y3-Y4))))

v_m_PS*h_m

% Reduced radius (should follow eps << 1)
eps_PS = a*(2*v_c)/(v_m_PS*h_m)

%% Calculate properties at barrier

% Calculate viscous drag at membrane
drag_m = 4*pi*v_m_PS*h_m / (log(2/(a*(2*v_c)/(v_m_PS*h_m))) - em + 4*(a*(2*v_c)/(v_m_PS*h_m))/pi - (((a*(2*v_c)/(v_m_PS*h_m))^2)/2)*log(2/(a*(2*v_c)/(v_m_PS*h_m)))) / (1 - (((a*(2*v_c)/(v_m_PS*h_m))^3)/pi)*log(2/(a*(2*v_c)/(v_m_PS*h_m))) + (c1*(a*(2*v_c)/(v_m_PS*h_m))^b1)/(1+c2*(a*(2*v_c)/(v_m_PS*h_m))^b2))

% Intersecting functions
w = [4000*v_c:0.001*v_c:4050*v_c];

Y5 = w*(4*pi*h_b)/(3*drag_m);
Y6 = (log(2./(a*(2*v_c)./(w*h_b))) - em + 4*(a*(2*v_c)./(w*h_b))/pi - (((a*(2*v_c)./(w*h_b)).^2)/2).*log(2./(a*(2*v_c)./(w*h_b)))) ./ (1 - (((a*(2*v_c)./(w*h_b)).^3)/pi).*log(2./(a*(2*v_c)./(w*h_b))) + (c1*(a*(2*v_c)./(w*h_b)).^b1)/(1+c2*(a*(2*v_c)./(w*h_b)).^b2));

% Plot intersection
figure(3); clf;
plot(w,Y5,w,Y6);
axis([w(1) w(end) 7.75 7.85]);

yL = get(gca,'YLim');
line([v_c v_c],yL,'Color','r');

v_b_PS = w(find(Y5==Y6+min(abs(Y5-Y6))))

v_b_PS*h_b
