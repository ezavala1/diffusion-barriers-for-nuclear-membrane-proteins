%% Script for importing and processing output from Yeast nuclei bleaching virtual experiments

clear

% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Project - Yeast/expdata/Exp data JCB - Matlab format/');

% Load data from measurements reported on JCB
src = importdata('Late_Src1_experimental.mat');

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
% ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/LA - Src/barrposition_bridge/';
% ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/LA - Src/barrposition_rings/';
ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/LA - Src/barrposition_1ring/';

%% Scroll over all bleaching spot positions

CP_mean = zeros(11,1);
 CP_err = zeros(11,1);

for spot = 1:11
  
  % Plot FLIP experiments with error bars
  figure(spot); clf;
  set(gcf,'Color','white');

  % Experimental data
  errorbar(src(:,1),src(:,3),src(:,4),'-x','Color','black');  % mother lobe
  hold on
  errorbar(src(:,1),src(:,5),src(:,6),'-x','Color','black');  % daughter lobe
  hold on

  % Select folder containing .txt Smoldyn output files
  pfolder = strcat('spot_',num2str(spot));

  % Set path to where output*.txt files are contained
  addpath([ppath pfolder])

  % Sort the mother and daughter lobe data files
  pvarM = '/*M.txt';
  pvarD = '/*D.txt';
  listingM = dir([ppath pfolder pvarM]);
  listingD = dir([ppath pfolder pvarD]);

  % Import data from files
  n=size(listingM,1);
  dataM=[];
  dataD=[];
  for i = 1:n
    dataM = cat(3,dataM,importdata(listingM(i,1).name,' '));
    dataD = cat(3,dataD,importdata(listingD(i,1).name,' '));
  end

  % Time information
  time = mean(dataM(:,1),3)-50;

  % Scale to 100% fluorescence equivalency
  mother=[];
  daughter=[];
  tt=26;     % thermalization threshold, bleaching reaction starts at this time-point
  for j = 1:n
      mother = cat(2,mother,(dataM(:,2,j)+dataM(:,5,j))*100/(dataM(tt,2,j)+dataM(tt,5,j)));
    daughter = cat(2,daughter,(dataD(:,2,j)+dataD(:,5,j))*100/(dataD(tt,2,j)+dataD(tt,5,j)));
  end

  % CP calculation parameters
  CP_sim = zeros(n,11);
  CP_sim_inv = zeros(n,11);
  th = 70; %Percentage fluorescence at which time will be registered
  xL = get(gca,'XLim');
  line(xL,[th th],'LineStyle','--','Color','r');

  for cell = 1:n
    % Plot each individual simulation
    h(1) = plot(time,mother(:,cell),'-o','Color',[1 0 0]);
    h(2) = plot(time,daughter(:,cell),'-o','Color',[0 0.5 0]);
    
    % Fitting a spline to simulation
    f_sim_M = fit(time,mother(:,cell),'smoothingspline');
    plot(f_sim_M(0:time(end)),'Color','cyan','LineWidth',2);
    f_sim_D = fit(time,daughter(:,cell),'smoothingspline');
    plot(f_sim_D(0:time(end)),'Color','yellow','LineWidth',2);
    
    % Calculate CP
    tc_sim_M = find(abs(f_sim_M(0:0.001:time(end))-th)==min(abs(f_sim_M(0:0.001:time(end))-th)))/1000;
    tc_sim_D = find(abs(f_sim_D(0:0.001:time(end))-th)==min(abs(f_sim_D(0:0.001:time(end))-th)))/1000;
    CP_sim(cell,spot) = tc_sim_D/tc_sim_M;
    CP_sim_inv(cell,spot) = tc_sim_M/tc_sim_D;
  end

  % Plotting parameters
  axis([0 time(end) 0 100]);
%   title(['\fontsize{14}Bleachspot relative position = ',num2str((spot-1)*10),'%      Domain class = Bridge']);
%   title(['\fontsize{14}Bleachspot relative position = ',num2str((spot-1)*10),'%      Domain class = Rings']);
  title(['\fontsize{14}Bleachspot relative position = ',num2str((spot-1)*10),'%      Domain class = Central Ring']);
  xlabel('\fontsize{12}Time (s)');
  ylabel('\fontsize{12}% Fluorescence');
  legend(h,'mother','daughter'); legend('boxoff');

  % Calculate statistics
  CP_mean(spot) = mean(CP_sim(:,spot),1);
   CP_err(spot) = std(CP_sim(:,spot),0,1);
  CPinv_mean(spot) = mean(CP_sim_inv(:,spot),1);
   CPinv_err(spot) = std(CP_sim_inv(:,spot),0,1);

end

%% Plot CP

figure(12); clf;  % Bridge
% figure(13); clf;  % Rings
set(gcf,'Color','white');

% From experiment
% Set path to where output .txt file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Project - Yeast/expdata/');
load expCPs_Src.txt
handle(1) = errorbar(expCPs_Src(1,:),expCPs_Src(2,:),expCPs_Src(3,:),'-x','Color','black','LineWidth',1.2,'MarkerSize',10);
hold on
errorbar(expCPs_Src(1,:),expCPs_Src(4,:),expCPs_Src(5,:),'-x','Color','black','LineWidth',1.2,'MarkerSize',10);
hold on

% From simulation
handle(2) = errorbar(0:10:100,CP_mean,CP_err,'-o','Color','blue','LineWidth',2,'MarkerFaceColor','blue');
hold on
handle(3) = errorbar(0:10:100,CPinv_mean,CPinv_err,'-o','Color',[1 0.6 0.2],'LineWidth',2,'MarkerFaceColor',[1 0.6 0.2]);
hold on

% Plotting parameters
axis([-20 110 0 45]);
% title({'\fontsize{16}Src1-GFP';['\fontsize{14}Domain class: Homogeneous Bridge']});
% title({'\fontsize{16}Src1-GFP';['\fontsize{14}Domain class: Parallel Rings']});
title({'\fontsize{16}Src1-GFP';['\fontsize{14}Domain class: Central Ring']});
xlabel('\fontsize{14}% Total bridge length');
ylabel('\fontsize{14}CP');
legend(handle,'exp','CP','1/CP'); legend('boxon');

%% Save Figure

% saveas(12,'CPs_Src1-bridge','epsc2');
% saveas(12,'CPs_Src1-rings','epsc2');
saveas(12,'CPs_Src1-1ring','epsc2');
