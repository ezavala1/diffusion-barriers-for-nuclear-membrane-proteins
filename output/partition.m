%% Script for calculating partition and transmission coefficient.

clear

% Select protein and folder containing .txt Smoldyn output files
hull = 'EA - Nsg';                 % Choose among 'EA - Nup', 'EA - Nsg', 'EA - Src', 'LA - Nup', 'LA - Nsg' and 'LA - Src'
domain = 'neck';                   % Choose among 'neck', 'rings' or 'bridge'

D = 0.1;    % case for Nsg1 and Src1
%D = 0.0566; % case for Nup (ONM)
%D = 0.0393; % case for Nup (ONM + INM)

% EA - neck
pfolder = 'sphin_barr-db_0.1-p_0.1500';     % NSG1
%pfolder = 'sphin_barr-db_0.1-p_0.070';      % SRC1
%pfolder = 'sphin_barr-db_0.0566-p_0.016';   % NUP (ONM)
%pfolder = 'sphin_barr-db_0.0393-p_0.015';   % NUP (ONM + INM)

% LA - neck
%pfolder = 'sphin_barr-db_0.1-p_0.035-1R_300nm';        % NSG1
%pfolder = 'sphin_barr-db_0.1-p_0.070-1R_300nm';        % SRC1
%pfolder = 'sphin_barr-db_0.0566-p_0.016-1R_100nm';     % NUP (ONM)
%pfolder = 'sphin_barr-db_0.0393-p_0.015-1R_100nm';     % NUP (ONM + INM)

% LA - rings
%pfolder = 'rings-db_0.1-p_0.10';       % NSG1
%pfolder = 'rings-db_0.1-p_0.30';       % SRC1
%pfolder = 'rings-db_0.0566-p_0.20';    % NUP (ONM)
%pfolder = 'rings-db_0.0393-p_0.20';    % NUP (ONM + INM)

% LA - bridge
%pfolder = 'homogen-db_0.1-p_0.15';     % NSG1
%pfolder = 'homogen-db_0.1-p_0.35';     % SRC1
%pfolder = 'homogen-db_0.0566-p_0.30';   % NUP (ONM)
%pfolder = 'homogen-db_0.0393-p_0.30';   % NUP (ONM + INM)

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
ppath = strcat('/Users/zavalaeder/Dropbox/Project - Yeast/output/',hull,'/');
addpath([ppath pfolder])

% Sort the mother and daughter lobe data files
pvarM = '/*M.txt';
pvarD = '/*D.txt';
listingM = dir([ppath pfolder pvarM]);
listingD = dir([ppath pfolder pvarD]);

% Import data from files
n=size(listingM,1);
dataM=[];
dataD=[];
for i = 1:n
  dataM = cat(3,dataM,importdata(listingM(i,1).name,' '));
  dataD = cat(3,dataD,importdata(listingD(i,1).name,' '));
end

%% Retrieve data

% Time information
time = mean(dataM(:,1),3);

% Concatenate particle numbers inside and outside of the sphingolipid domain for all geometries
inside=[];
outside=[];
for j = 1:n
  [tot_A,dom_A] = hull_area(hull,domain,j);
  if hull == 'EA - Nup' | hull == 'LA - Nup'
     inside = cat(2,inside,(dataM(:,4,j)+dataM(:,5,j)+dataD(:,4,j)+dataD(:,5,j))/dom_A);
    outside = cat(2,outside,(dataM(:,2,j)+dataM(:,3,j)+dataD(:,2,j)+dataD(:,3,j))/(tot_A - dom_A));
  else
     inside = cat(2,inside,(dataM(:,5,j)+dataM(:,6,j)+dataD(:,5,j)+dataD(:,6,j))/dom_A);
    outside = cat(2,outside,(dataM(:,2,j)+dataM(:,3,j)+dataM(:,4,j)+dataD(:,2,j)+dataD(:,3,j)+dataD(:,4,j))/(tot_A - dom_A));
  end
end

%% Calculate statistics over all geometries

 inside_mean = mean(inside,2);
outside_mean = mean(outside,2);
  inside_err = std(inside,0,2);
 outside_err = std(outside,0,2);

C_i = mean(inside_mean(floor(end/2):end));
C_o = mean(outside_mean(floor(end/2):end));

C_i_err = mean(inside_err(floor(end/2):end));
C_o_err = mean(outside_err(floor(end/2):end));

%% Calculate partition and transmission coefficient

K = C_i/C_o;
dK = K*sqrt((C_i_err/C_i)^2+(C_o_err/C_o)^2);

% Select case for domain shape
switch domain
  case 'bridge'
    lambda = 2.85;
    lambda_err = 0.83;
  case 'rings'
    lambda = 1.45;
    lambda_err = 0;
  case 'neck'
    lambda = 0.3;
    lambda_err = 0;
  otherwise
    warning('Unspecified domain');
end

theta = K*D/lambda;
theta_err = theta*sqrt((dK/K)^2+(lambda_err/lambda)^2);

%% Plot mean concentration of particles inside and outside the sphingolipid domain vs time
figure(1); clf;
set(gcf,'Color','white');

h(1) = errorbar(time,inside_mean,inside_err,'-o','Color',[1 0 0]);
hold on
h(2) = errorbar(time,outside_mean,outside_err,'-o','Color',[0 0.5 0]);
hold on

axis([0 time(end) 0 1.3*max(max(inside_mean),max(outside_mean))]);
set(gca,'FontSize',16);

title({'\fontsize{20}Time averaged concentrations';['\fontsize{18}n_{cells} = ',num2str(n),'    -    Case = ',hull,'  ',pfolder]});
xlabel('\fontsize{14}Time (s)');
ylabel('\fontsize{14}Mean concentration');
legend(h,strcat('C_{i} = ',num2str(C_i)),strcat('C_{o} = ',num2str(C_o))); legend('boxoff');

%% Display data

sprintf('K = %g %s %g',K,'+-',dK)   % Partition coefficient
sprintf('Theta = %g %s %g',theta,'+-',theta_err)   % Transmission coefficient

%% Save Data and Figures

% filename = strcat(hull,'- ',pfolder,'.mat');
% save(filename,'K','dK','theta','theta_err')

% saveas(1,'partition','epsc2');
