%% Script for importing and processing output from Yeast nuclei bleaching virtual experiments

clear

% Select folder containing .txt Smoldyn output files
pfolder = 'd_1.9-p_1.0-volexc_no-ts_0.00010';

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/LA - TetR/';
addpath([ppath pfolder])

% Sort the mother and daughter lobe data files
pvar = '/*.txt';
listing = dir([ppath pfolder pvar]);

% Import data from files
n=size(listing,1);
data=[];
for i = 1:n
  data = cat(3,data,importdata(listing(i,1).name,' '));
end

% Time information
time = mean(data(:,1),3);

% Scale to 100% fluorescence equivalency
mother=[];
daughter=[];
tt=1;     % thermalization threshold, bleaching reaction starts at this time-point
for j = 1:n
    mother = cat(2,mother,data(:,2,j)*100/data(tt,2,j));
  daughter = cat(2,daughter,data(:,4,j)*100/data(tt,4,j));
end

% Calculate statistics
  mother_mean = mean(mother,2);
daughter_mean = mean(daughter,2);
   mother_err = std(mother,0,2);
 daughter_err = std(daughter,0,2);

% Load data from measurements reported on JCB to compare against
% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Project - Yeast/expdata/Exp data JCB - Matlab format/');
tetR = importdata('Late_TetR_experimental.mat');

% Plot FLIP experiments vs simulation with error bars
figure(1); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(tetR(:,1),tetR(:,3),tetR(:,4),'-x','Color','black','LineWidth',0.5);  % mother lobe
hold on
errorbar(tetR(:,1),tetR(:,5),tetR(:,6),'-x','Color','black','LineWidth',0.5);  % daughter lobe
hold on

% Simulation
h(1) = errorbar(time,mother_mean,mother_err,'-o','Color',[1 0 0],'LineWidth',0.5);
hold on
h(2) = errorbar(time,daughter_mean,daughter_err,'-o','Color',[0 0.5 0],'LineWidth',0.5);
hold on

set(gca,'FontSize',40);
box off

axis([0 100 0 100]);
% title({'\fontsize{14}TetR-GFP in Late Anaphase';['\fontsize{12}n_{cells} = ',num2str(n/5),'    -    Permeability = 1']});
% xlabel('\fontsize{12}Time (s)');
% ylabel('\fontsize{12}% Fluorescence');
% legend(h,'mother','daughter'); legend('boxoff');

text(1,0.9,'P = 100%','Units','normalized','HorizontalAlignment','right','FontSize',40)

%% Save Figure

saveas(1,'/Users/zavalaeder/Dropbox/Project - Yeast/paper/figures/fig_S1/LA_TetR-P_100','epsc2');
