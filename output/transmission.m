%% Script for plotting transmission coefficient

clear

% Select path to folder containing .mat output files
ppath = strcat('/Users/zavalaeder/Dropbox/Project - Yeast/output/permeabilities');
addpath([ppath])

% Sort the Nsg and Src data files
file_EA_neck = '/EA*.mat';
file_LA_neck = '/LA*sphin*.mat';
file_LA_rings = '/LA*rings*.mat';
file_LA_bridge = '/LA*homogen*.mat';

list_EA_neck = dir([ppath file_EA_neck]);
list_LA_neck = dir([ppath file_LA_neck]);
list_LA_rings = dir([ppath file_LA_rings]);
list_LA_bridge = dir([ppath file_LA_bridge]);

% Import data from files
n=size(list_EA_neck,1);
data_EA_neck=[];
data_LA_neck=[];
data_LA_rings=[];
data_LA_bridge=[];
for i = 1:n
  data_EA_neck = cat(1,data_EA_neck,importdata(list_EA_neck(i,1).name,' '));
  data_LA_neck = cat(1,data_LA_neck,importdata(list_LA_neck(i,1).name,' '));
  data_LA_rings = cat(1,data_LA_rings,importdata(list_LA_rings(i,1).name,' '));
  data_LA_bridge = cat(1,data_LA_bridge,importdata(list_LA_bridge(i,1).name,' '));
end

%% Plot transmission coefficients

y = [data_EA_neck(1).theta,data_EA_neck(4).theta,data_EA_neck(3).theta,data_EA_neck(2).theta,
     data_LA_neck(1).theta,data_LA_neck(4).theta,data_LA_neck(3).theta,data_LA_neck(2).theta,
     data_LA_rings(1).theta,data_LA_rings(4).theta,data_LA_rings(3).theta,data_LA_rings(2).theta,
     data_LA_bridge(1).theta,data_LA_bridge(4).theta,data_LA_bridge(3).theta,data_LA_bridge(2).theta];

dy = [data_EA_neck(1).theta_err,data_EA_neck(4).theta_err,data_EA_neck(3).theta_err,data_EA_neck(2).theta_err,
      data_LA_neck(1).theta_err,data_LA_neck(4).theta_err,data_LA_neck(3).theta_err,data_LA_neck(2).theta_err,
      data_LA_rings(1).theta_err,data_LA_rings(4).theta_err,data_LA_rings(3).theta_err,data_LA_rings(2).theta_err,
      data_LA_bridge(1).theta_err,data_LA_bridge(4).theta_err,data_LA_bridge(3).theta_err,data_LA_bridge(2).theta_err];
 
figure(1); clf;
set(gcf,'Color','white');

h = barwitherr(dy,y);
colormap(summer);

ylim([0 1.5*max(max(y))]);
xlim([0.5 4.5]);

protein = {'Nsg1','Src1','NPC (ONM)','NPC (ONM + INM)'};
%domains = {'EA - Neck','LA - One ring','LA - Rings','LA - Bridge'};
%set(gca,'FontSize',16,'XTickLabel',domains);
domains = {' ',' ',' ',' '};
set(gca,'FontSize',30,'XTickLabel',domains,'YTick',[0:0.05:0.2]);

%title({'\fontsize{20}Sphingolipid domain permeability'});
%ylabel('\fontsize{14}Transmission coefficient ( {\mu} s{^-^1})');
lh = legend(h,protein,'Location','NorthOutside','Orientation','horizontal'); legend('boxoff');
set(lh,'FontSize',20)

%text(0.39,0.125,'*','Units','normalized','FontSize',16);
%text(0.434,0.16,'*','Units','normalized','FontSize',16);

%% Save Figures

%saveas(1,'transmission','epsc2');
