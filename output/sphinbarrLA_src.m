%% Script for importing and processing output from Yeast nuclei bleaching virtual experiments

clear

% Select folder containing .txt Smoldyn output files
pfolder = 'homogen-db_0.1-p_0.35';

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/output/LA - Src/';
addpath([ppath pfolder])

% Sort the mother and daughter lobe data files
pvarM = '/*M.txt';
pvarD = '/*D.txt';
listingM = dir([ppath pfolder pvarM]);
listingD = dir([ppath pfolder pvarD]);

% Import data from files
n=size(listingM,1);
dataM=[];
dataD=[];
for i = 1:n
  dataM = cat(3,dataM,importdata(listingM(i,1).name,' '));
  dataD = cat(3,dataD,importdata(listingD(i,1).name,' '));
end

% Time information
time = mean(dataM(:,1),3)-50;

% Scale to 100% fluorescence equivalency
mother=[];
daughter=[];
tt=26;     % thermalization threshold, bleaching reaction starts at this time-point
for j = 1:n
    mother = cat(2,mother,(dataM(:,2,j)+dataM(:,5,j))*100/(dataM(tt,2,j)+dataM(tt,5,j)));
  daughter = cat(2,daughter,(dataD(:,2,j)+dataD(:,5,j))*100/(dataD(tt,2,j)+dataD(tt,5,j)));
end

% Calculate statistics
  mother_mean = mean(mother,2);
daughter_mean = mean(daughter,2);
   mother_err = std(mother,0,2);
 daughter_err = std(daughter,0,2);

% Load data from measurements reported on JCB to compare against
% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Project - Yeast/expdata/Exp data JCB - Matlab format/');
src = importdata('Late_Src1_experimental.mat');

% Plot FLIP experiments vs simulation with error bars
figure(1); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(src(:,1),src(:,3),src(:,4),'-x','Color','black');  % mother lobe
hold on
errorbar(src(:,1),src(:,5),src(:,6),'-x','Color','black');  % daughter lobe
hold on

% Simulation
h(1) = errorbar(time,mother_mean,mother_err,'-o','Color',[1 0 0]);
hold on
h(2) = errorbar(time,daughter_mean,daughter_err,'-o','Color',[0 0.5 0]);
hold on

axis([0 time(end) 0 100]);
title({'\fontsize{14}Src1-GFP in Late Anaphase';['\fontsize{12}n_{cells} = ',num2str(n),'    -    Homogeneous domain   -   P_{in} = 0.35']});
xlabel('\fontsize{12}Time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend(h,'mother','daughter'); legend('boxoff');

%% Save Figure

saveas(1,'LA_Src1-sphinbarr-hom_domain','epsc2');

%% Fit an exponential to data

% From experiments
f_exp_M = fit(src(:,1),src(:,3),'exp2');
plot(f_exp_M(0:time(end)),'Color','blue','LineWidth',2);
f_exp_B = fit(src(:,1),src(:,5),'exp2');
plot(f_exp_B(0:time(end)),'Color','blue','LineWidth',2);

% From simulations
f_sim_M = fit(time,mother_mean,'smoothingspline');
plot(f_sim_M(0:time(end)),'Color','cyan','LineWidth',2);
f_sim_B = fit(time,daughter_mean,'smoothingspline');
plot(f_sim_B(0:time(end)),'Color','cyan','LineWidth',2);

%% Calculate CP

% Percentage fluorescence at which time will be registered
th = 70
%th = 100*(1-exp(-1))

xL = get(gca,'XLim');
line(xL,[th th],'LineStyle','--','Color','r');

% From experiments
tc_exp_M = find(abs(f_exp_M(0:0.001:time(end))-th)==min(abs(f_exp_M(0:0.001:time(end))-th)))/1000;
tc_exp_B = find(abs(f_exp_B(0:0.001:time(end))-th)==min(abs(f_exp_B(0:0.001:time(end))-th)))/1000;
CP_exp = tc_exp_B/tc_exp_M

% From simulations
tc_sim_M = find(abs(f_sim_M(0:0.001:time(end))-th)==min(abs(f_sim_M(0:0.001:time(end))-th)))/1000;
tc_sim_B = find(abs(f_sim_B(0:0.001:time(end))-th)==min(abs(f_sim_B(0:0.001:time(end))-th)))/1000;
CP_sim = tc_sim_B/tc_sim_M

divergence = abs(CP_exp-CP_sim)

