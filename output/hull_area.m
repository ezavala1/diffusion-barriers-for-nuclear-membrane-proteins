function [tot_A,dom_A] = hull_area(hull,domain,cell)

% Select file and path variable according to function input
switch hull
  case 'EA - Nup'
    file_1 = 'EADumbbellOut.txt';
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-EA/';
  case 'EA - Nsg'
    file_1 = 'EADumbbellOut.txt';
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-EA/';
  case 'EA - Src'
    file_1 = 'EADumbbellIn.txt';
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-EA/';
  case 'LA - Nup'
    file_1 = 'LADumbbellOut.txt';
    file_2 = strcat(domain,'.txt');
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-LA/';
  case 'LA - Nsg'
    file_1 = 'LADumbbellOut.txt';
    file_2 = strcat(domain,'.txt');
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-LA/';
  case 'LA - Src'
    file_1 = 'LADumbbellIn.txt';
    file_2 = strcat(domain,'.txt');
    ppath = '/Users/zavalaeder/Dropbox/Project - Yeast/geom-LA/';
  otherwise
    warning('Unexpected input in hull_area function');
end

% Select folder containing *.txt geometry defining files
pfolder = strcat('cell_',num2str(cell));

addpath([ppath pfolder])    % Set path to original data file

%% Read data of triangular panels and calculate total area

hull_file = fopen(file_1,'r');    % Open original data file and set permissions (e.g. read, write, etc.)
panels = textscan(hull_file,'%s %*s %f %f %f %f %f %f %f %f %f %*s','HeaderLines',4,'CommentStyle',{'neighbors','end_file'});

rows_1 = size(panels{10},1);  % all coordinates

tot_A = 0;

for i = 1:rows_1
  if strcmp([panels{1}(i)],'#panel')  % ignore spurious triangles
    continue
  else
    P1 = [panels{2}(i),panels{3}(i),panels{4}(i)];
    P2 = [panels{5}(i),panels{6}(i),panels{7}(i)];
    P3 = [panels{8}(i),panels{9}(i),panels{10}(i)];
    tot_A = tot_A + tri_area(P1,P2,P3);  % calculate area of triangle
  end
end

%% Read data of sphingolipid domains and calculate area

% Auxiliary data for bridge and neck dimensions
diam_bridge_out = 0.27;
 diam_bridge_in = 0.15;
  diam_neck_out = 0.6376;
   diam_neck_in = 0.5936;
     srw = 0.3;  % single ring width

if strcmp(domain,'neck')
  rows_2 = 2;
else
  domain_file = fopen(file_2,'r');    % Open original data file and set permissions (e.g. read, write, etc.)
  borders = textscan(domain_file,'%*s %*s %f %*f %*f %*f %*f %*f %*f %*f','HeaderLines',11,'CommentStyle','end_file');
  rows_2 = size(borders{1},1);  % all coordinates
end

if strcmp(domain,'bridge')  % If the domain is a bridge
  if hull == 'LA - Nsg' | hull == 'LA - Nup'
    dom_A = pi*diam_bridge_out*abs(2*borders{1}(1));
  elseif hull == 'LA - Src'
    dom_A = pi*diam_bridge_in*abs(2*borders{1}(1));
  end
else    % If the domain is a set of rings or single ring at neck
  switch hull
    case 'LA - Nup'
      if strcmp(domain,'neck')
        dom_A = pi*diam_bridge_out*srw/3*rows_2/2;
      else
        dom_A = pi*diam_bridge_out*srw*rows_2/2;
      end
    case 'LA - Nsg'
      dom_A = pi*diam_bridge_out*srw*rows_2/2;
    case 'LA - Src'
      dom_A = pi*diam_bridge_in*srw*rows_2/2;
    case 'EA - Nup'
      dom_A = pi*diam_neck_out*srw;
    case 'EA - Nsg'
      dom_A = pi*diam_neck_out*srw;
    case 'EA - Src'
      dom_A = pi*diam_neck_in*srw;
  end  
end

%% Close files

fclose('all');

return