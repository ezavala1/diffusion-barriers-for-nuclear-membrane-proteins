%%%%%%%%%%%%%%%%%%%%%%%%% Saffman-Delbruck model %%%%%%%%%%%%%%%%%%%%%%%%%

% The model describes the diffusion coefficient of a cylindrical inclusion
% of radius a in a membrane with thickness h and viscosity v_m, surrounded
% by bulk fluid with viscosity v_f

% SaffmanDelbruck 1975 PNAS 72(8) 3111-3113

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% All parameters in SI units

clear

format long

%% Global parameters

 kB = 1.3806e-23;  % Boltzmann constant
 em = 0.577215;      % Euler-Mascheroni constant
  T = 303.15;      % Temperature in at which FLIP experiments were carried out (30 C)
v_c = 11.967e-4;   % Bulk viscosity of cytoplasm in Pa s at 30 C
h_m = 4e-9;        % Membrane thickness
h_b = 5.4e-9;      % Domain thickness

%% Local parameters

% Internal parameters Petrov-Schwille approximation
c1 = 0.73761;
b1 = 2.74819;
c2 = 0.52119;
b2 = 0.61465;

Dm_NPC = 0.2e-12;   % Effective diffusion coefficient at membrane for NPC
 a_NPC = 49e-9;     % NPC radii

  sv_m = 1.4791e-8;   % Surface viscosity outside of the sphingolipid domain
sv_ONM = 3.4083e-8;   % Surface viscosity inside of the sphingolipid domain (only ONM)
 sv_NE = 5.2816e-8;   % Surface viscosity inside of the sphingolipid domain (ONM + INM)
 
%% Calculate diffusion rates

sv = sv_NE;

eps = a_NPC*2*v_c/sv

% Calculate viscous drag
drag_m = 4*pi*sv / (log(2/eps) - em + 4*eps/pi - ((eps^2)/2)*log(2/eps)) / (1 - ((eps^3)/pi)*log(2/eps) + (c1*eps^b1)/(1+c2*eps^b2))

% Calculate diffusion rate
D = kB*T/drag_m