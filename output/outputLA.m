%% Script for importing and processing output from Yeast nuclei bleaching virtual experiments

clear

% Select folder containing .txt Smoldyn output files
pfolder = 'LA - All-volexc_no';

% Set path variable to where output*.txt files are contained. Use 'zavalaeder' if using MacPro at OIST
ppath = '/Users/zavalaeder/Desktop/Project - Yeast/output/';
addpath([ppath pfolder])

% Sort the mother and daughter lobe data files
pvarM = '/*M.txt';
pvarD = '/*D.txt';
listingM = dir([ppath pfolder pvarM]);
listingD = dir([ppath pfolder pvarD]);

% Import data from files
n=size(listingM,1);
dataM=[];
dataD=[];
for i = 1:n
  dataM = cat(3,dataM,importdata(listingM(i,1).name,' '));
  dataD = cat(3,dataD,importdata(listingD(i,1).name,' '));
end

% Time information
time = mean(dataM(:,1),3)-50;

% Scale to 100% fluorescence equivalency
nup_M=[];
nup_D=[];
nsg_M=[];
nsg_D=[];
src_M=[];
src_D=[];
tt=26;     % thermalization threshold, bleaching reaction starts at this time-point
for j = 1:n

  nup_M = cat(2,nup_M,dataM(:,2,j)*100/dataM(tt,2,j));
  nup_D = cat(2,nup_D,dataD(:,2,j)*100/dataD(tt,2,j));
  
  nsg_M = cat(2,nsg_M,dataM(:,3,j)*100/dataM(tt,3,j));
  nsg_D = cat(2,nsg_D,dataD(:,3,j)*100/dataD(tt,3,j));

  src_M = cat(2,src_M,dataM(:,4,j)*100/dataM(tt,4,j));
  src_D = cat(2,src_D,dataD(:,4,j)*100/dataD(tt,4,j));

end

% Calculate statistics
  nup_M_mean = mean(nup_M,2);
  nup_D_mean = mean(nup_D,2);
   nup_M_err = std(nup_M,0,2);
   nup_D_err = std(nup_D,0,2);

  nsg_M_mean = mean(nsg_M,2);
  nsg_D_mean = mean(nsg_D,2);
   nsg_M_err = std(nsg_M,0,2);
   nsg_D_err = std(nsg_D,0,2);

  src_M_mean = mean(src_M,2);
  src_D_mean = mean(src_D,2);
   src_M_err = std(src_M,0,2);
   src_D_err = std(src_D,0,2);

   
% Load data from measurements reported on JCB to compare against
% Set path to where output .mat file is contained. Use 'zavalaeder' if using MacPro at OIST
addpath('/Users/zavalaeder/Dropbox/Yeast/expdata/Exp data JCB - Matlab format/');
nup = importdata('Late_Nup49_experimental.mat');
nsg = importdata('Late_Nsg1_experimental.mat');
src = importdata('Late_Src1_experimental.mat');

%% NUP

% Plot FLIP experiments vs simulation with error bars
figure(1); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(nup(:,1),nup(:,3),nup(:,4),'-x','Color','black');  % mother lobe
hold on
errorbar(nup(:,1),nup(:,5),nup(:,6),'-x','Color','black');  % daughter lobe
hold on

% Simulation
f(1) = errorbar(time,nup_M_mean,nup_M_err,'-o','Color',[1 0 0]);
hold on
f(2) = errorbar(time,nup_D_mean,nup_D_err,'-o','Color',[0 0.5 0]);
hold on

axis([0 150 0 100]);
title({'\fontsize{14}Nup49-GFP in Late Anaphase';['\fontsize{12}n_{cells} = ',num2str(n),'    -    Permeability = 0.0005']});
xlabel('\fontsize{12}Time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend(f,'mother','daughter'); legend('boxoff');

%% NSG

% Plot FLIP experiments vs simulation with error bars
figure(2); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(nsg(:,1),nsg(:,3),nsg(:,4),'-x','Color','black');  % mother lobe
hold on
errorbar(nsg(:,1),nsg(:,5),nsg(:,6),'-x','Color','black');  % daughter lobe
hold on

% Simulation
g(1) = errorbar(time,nsg_M_mean,nsg_M_err,'-o','Color',[1 0 0]);
hold on
g(2) = errorbar(time,nsg_D_mean,nsg_D_err,'-o','Color',[0 0.5 0]);
hold on

axis([0 150 0 100]);
title({'\fontsize{14}Nsg1-GFP in Late Anaphase';['\fontsize{12}n_{cells} = ',num2str(n),'    -    Permeability = 0.0002']});
xlabel('\fontsize{12}Time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend(g,'mother','daughter'); legend('boxoff');

%% SRC

% Plot FLIP experiments vs simulation with error bars
figure(3); clf;
set(gcf,'Color','white');

% Experimental data
errorbar(src(:,1),src(:,3),src(:,4),'-x','Color','black');  % mother lobe
hold on
errorbar(src(:,1),src(:,5),src(:,6),'-x','Color','black');  % daughter lobe
hold on

% Simulation
h(1) = errorbar(time,src_M_mean,src_M_err,'-o','Color',[1 0 0]);
hold on
h(2) = errorbar(time,src_D_mean,src_D_err,'-o','Color',[0 0.5 0]);
hold on

axis([0 150 0 100]);
title({'\fontsize{14}Src1-GFP in Late Anaphase';['\fontsize{12}n_{cells} = ',num2str(n),'    -    Permeability = 0.0005']});
xlabel('\fontsize{12}Time (s)');
ylabel('\fontsize{12}% Fluorescence');
legend(h,'mother','daughter'); legend('boxoff');
